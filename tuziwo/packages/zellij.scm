(define-module (tuziwo packages zellij)
  #:use-module
  ((guix licenses) #:prefix license:)
  #:use-module
  (guix build-system cargo)
  #:use-module
  (guix download)
  #:use-module
  (guix packages))

(define rust-addr2line-0.17
  (package
    (name "rust-addr2line")
    (version "0.17.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "addr2line" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0sw16zqy6w0ar633z69m7lw6gb0k1y7xj3387a8wly43ij5div5r"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-gimli" ,rust-gimli-0.26))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-adler-1
  (package
    (name "rust-adler")
    (version "1.0.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "adler" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1zim79cvzd5yrkzl3nyfx0avijwgk9fqv3yrscdy1cc79ih02qpj"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-aead-0.3
  (package
    (name "rust-aead")
    (version "0.3.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "aead" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0c8388alvivcj4qkxgh4s4l6fbczn3p8wc0pnar6crlfvcdmvjbz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-generic-array" ,rust-generic-array-0.14))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-aes-0.6
  (package
    (name "rust-aes")
    (version "0.6.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "aes" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0q85mw70mgr4glza9y9lrs9nxfa1cdcqzfk6wx0smb3623pr2hw8"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cipher" ,rust-cipher-0.2)
         ("rust-aesni" ,rust-aesni-0.10)
         ("rust-aes-soft" ,rust-aes-soft-0.6))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-aes-gcm-0.8
  (package
    (name "rust-aes-gcm")
    (version "0.8.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "aes-gcm" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1nl8iwlh209y1vj9n2lm1a70i69clvg2z6x69bi4dgdrpgxbay2j"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-subtle" ,rust-subtle-2)
         ("rust-ghash" ,rust-ghash-0.3)
         ("rust-ctr" ,rust-ctr-0.6)
         ("rust-cipher" ,rust-cipher-0.2)
         ("rust-aes" ,rust-aes-0.6)
         ("rust-aead" ,rust-aead-0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-aes-soft-0.6
  (package
    (name "rust-aes-soft")
    (version "0.6.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "aes-soft" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0wj0fi2pvmlw09yvb1aqf0mfkzrfxmjsf90finijh255ir4wf55y"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-opaque-debug" ,rust-opaque-debug-0.3)
         ("rust-cipher" ,rust-cipher-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-aesni-0.10
  (package
    (name "rust-aesni")
    (version "0.10.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "aesni" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1kmh07fp9hbi1aa8dr2rybbgw8vqz6hjmk34c4w7sbscx7si2bpa"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-opaque-debug" ,rust-opaque-debug-0.3)
         ("rust-cipher" ,rust-cipher-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-ahash-0.7
  (package
    (name "rust-ahash")
    (version "0.7.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ahash" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0isw672fiwx8cjl040jrck6pi85xcszkz6q0xsqkiy6qjl31mdgw"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-version-check" ,rust-version-check-0.9)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-getrandom" ,rust-getrandom-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-aho-corasick-0.7
  (package
    (name "rust-aho-corasick")
    (version "0.7.20")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "aho-corasick" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1b3if3nav4qzgjz9bf75b2cv2h2yisrqfs0np70i38kgz4cn94yc"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-memchr" ,rust-memchr-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-aho-corasick-1
  (package
    (name "rust-aho-corasick")
    (version "1.0.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "aho-corasick" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "014ddyrlbwg18m74fa52wrfik8y3pzhwqg811yvsyc8cjb70iz37"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-memchr" ,rust-memchr-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-ansi-term-0.12
  (package
    (name "rust-ansi-term")
    (version "0.12.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ansi_term" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1ljmkbilxgmhavxvxqa7qvm6f3fjggi7q2l3a72q9x0cxjvrnanm"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-winapi" ,rust-winapi-0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-anyhow-1
  (package
    (name "rust-anyhow")
    (version "1.0.71")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "anyhow" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1f6rm4c9nlp0wazm80wlw45zpmb48nv24x2227zyidz0y0c0czcw"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-backtrace" ,rust-backtrace-0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-arc-swap-1
  (package
    (name "rust-arc-swap")
    (version "1.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "arc-swap" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "07sb99f18spqmjx7f4cmqx7hc8ayspcmw9shl4zjvf300ki8rmy5"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-arrayvec-0.5
  (package
    (name "rust-arrayvec")
    (version "0.5.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "arrayvec" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "12q6hn01x5435bprwlb7w9m7817dyfq55yrl4psygr78bp32zdi3"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-arrayvec-0.7
  (package
    (name "rust-arrayvec")
    (version "0.7.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "arrayvec" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1mjl8jjqxpl0x7sm9cij61cppi7yi38cdrd1l8zjw7h7qxk2v9cd"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-async-attributes-1
  (package
    (name "rust-async-attributes")
    (version "1.1.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "async-attributes" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1rcnypqgmlcw9vwh0fk8bivvz8p5v8acy0zd2njdv6yxyiwkw853"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-syn" ,rust-syn-1)
         ("rust-quote" ,rust-quote-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-async-channel-1
  (package
    (name "rust-async-channel")
    (version "1.8.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "async-channel" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0cv8ha6y7s6scsgjipn7ciybry9g57rklw8404igrksw7vlgwing"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-event-listener" ,rust-event-listener-2)
         ("rust-concurrent-queue"
          ,rust-concurrent-queue-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-async-executor-1
  (package
    (name "rust-async-executor")
    (version "1.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "async-executor" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0rd9sd0rksvjwx4zzy6c69qcd7bwp3z42rpiiizfnbm2w2srn7w7"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-slab" ,rust-slab-0.4)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-futures-lite" ,rust-futures-lite-1)
         ("rust-fastrand" ,rust-fastrand-1)
         ("rust-concurrent-queue"
          ,rust-concurrent-queue-1)
         ("rust-async-task" ,rust-async-task-4))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-async-global-executor-2
  (package
    (name "rust-async-global-executor")
    (version "2.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "async-global-executor" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0xmpcx23k8k887bzb97p9n0i2gf6rxpcdvpq9542kg97vzbzbdpi"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-once-cell" ,rust-once-cell-1)
         ("rust-futures-lite" ,rust-futures-lite-1)
         ("rust-blocking" ,rust-blocking-1)
         ("rust-async-lock" ,rust-async-lock-2)
         ("rust-async-io" ,rust-async-io-1)
         ("rust-async-executor" ,rust-async-executor-1)
         ("rust-async-channel" ,rust-async-channel-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-async-io-1
  (package
    (name "rust-async-io")
    (version "1.13.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "async-io" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1byj7lpw0ahk6k63sbc9859v68f28hpaab41dxsjj1ggjdfv9i8g"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-waker-fn" ,rust-waker-fn-1)
         ("rust-socket2" ,rust-socket2-0.4)
         ("rust-slab" ,rust-slab-0.4)
         ("rust-rustix" ,rust-rustix-0.37)
         ("rust-polling" ,rust-polling-2)
         ("rust-parking" ,rust-parking-2)
         ("rust-log" ,rust-log-0.4)
         ("rust-futures-lite" ,rust-futures-lite-1)
         ("rust-concurrent-queue"
          ,rust-concurrent-queue-2)
         ("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-autocfg" ,rust-autocfg-1)
         ("rust-async-lock" ,rust-async-lock-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-async-lock-2
  (package
    (name "rust-async-lock")
    (version "2.8.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "async-lock" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0asq5xdzgp3d5m82y5rg7a0k9q0g95jy6mgc7ivl334x7qlp4wi8"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-event-listener" ,rust-event-listener-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-async-process-1
  (package
    (name "rust-async-process")
    (version "1.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "async-process" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "037ns7dazk1z0vdpnzh2bvrnxxzd604pzl47765cgs141bihcb6g"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-winapi" ,rust-winapi-0.3)
         ("rust-signal-hook" ,rust-signal-hook-0.3)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-futures-lite" ,rust-futures-lite-1)
         ("rust-event-listener" ,rust-event-listener-2)
         ("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-blocking" ,rust-blocking-1)
         ("rust-async-io" ,rust-async-io-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-async-std-1
  (package
    (name "rust-async-std")
    (version "1.11.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "async-std" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "13402qlkg0lbq1n92vd3vmq3n2hw6wbabcp8rlvdnp4wff8hjn2j"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-wasm-bindgen-futures"
          ,rust-wasm-bindgen-futures-0.4)
         ("rust-slab" ,rust-slab-0.4)
         ("rust-pin-utils" ,rust-pin-utils-0.1)
         ("rust-pin-project-lite"
          ,rust-pin-project-lite-0.2)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-num-cpus" ,rust-num-cpus-1)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-log" ,rust-log-0.4)
         ("rust-kv-log-macro" ,rust-kv-log-macro-1)
         ("rust-gloo-timers" ,rust-gloo-timers-0.2)
         ("rust-futures-lite" ,rust-futures-lite-1)
         ("rust-futures-io" ,rust-futures-io-0.3)
         ("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-futures-channel"
          ,rust-futures-channel-0.3)
         ("rust-crossbeam-utils"
          ,rust-crossbeam-utils-0.8)
         ("rust-async-process" ,rust-async-process-1)
         ("rust-async-lock" ,rust-async-lock-2)
         ("rust-async-io" ,rust-async-io-1)
         ("rust-async-global-executor"
          ,rust-async-global-executor-2)
         ("rust-async-channel" ,rust-async-channel-1)
         ("rust-async-attributes"
          ,rust-async-attributes-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-async-task-4
  (package
    (name "rust-async-task")
    (version "4.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "async-task" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1affldc0d5cfgsqsw74gvbl41qfm1479hjg05307y40pv226ls9h"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-async-trait-0.1
  (package
    (name "rust-async-trait")
    (version "0.1.56")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "async-trait" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "05lpn4164icjwarh2ax8whj0fzkkxz8cahps5l5snbkxyqlqikwn"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-syn" ,rust-syn-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-atomic-waker-1
  (package
    (name "rust-atomic-waker")
    (version "1.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "atomic-waker" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0ansiq5vlw684fhks2x4a4is2rqlbv50q5mi8x0fxxvx5q2p8lq6"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-atty-0.2
  (package
    (name "rust-atty")
    (version "0.2.14")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "atty" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1s7yslcs6a28c5vz7jwj63lkfgyx8mx99fdirlhi9lbhhzhrpcyr"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-winapi" ,rust-winapi-0.3)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-hermit-abi" ,rust-hermit-abi-0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-autocfg-1
  (package
    (name "rust-autocfg")
    (version "1.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "autocfg" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1ylp3cb47ylzabimazvbz9ms6ap784zhb6syaz6c1jqpmcmq0s6l"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-backtrace-0.3
  (package
    (name "rust-backtrace")
    (version "0.3.65")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "backtrace" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0qggp0d8pbw5vfnpm0r7lrn6wmh5yjiz4yc4bzynb8l26i2pv88i"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-rustc-demangle" ,rust-rustc-demangle-0.1)
         ("rust-object" ,rust-object-0.28)
         ("rust-miniz-oxide" ,rust-miniz-oxide-0.5)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-cc" ,rust-cc-1)
         ("rust-addr2line" ,rust-addr2line-0.17))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-backtrace-ext-0.2
  (package
    (name "rust-backtrace-ext")
    (version "0.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "backtrace-ext" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0l4xacjnx4jrn9k14xbs2swks018mviq03sp7c1gn62apviywysk"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-backtrace" ,rust-backtrace-0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-base-x-0.2
  (package
    (name "rust-base-x")
    (version "0.2.11")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "base-x" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0w02sdqvg7zwm91agb2phikw4ri8jmncw32paxsv8ra1jv8ckfsc"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-base64-0.13
  (package
    (name "rust-base64")
    (version "0.13.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "base64" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1z82g23mbzjgijkpcrilc7nljpxpvpf7zxf6iyiapkgka2ngwkch"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-base64-0.21
  (package
    (name "rust-base64")
    (version "0.21.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "base64" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0sidjip5b33sr6w7kasfj9qxpbda41nw0x4gjjk55g55a6mdv954"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-bitflags-1
  (package
    (name "rust-bitflags")
    (version "1.3.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bitflags" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "12ki6w8gn1ldq7yz9y680llwk5gmrhrzszaa17g1sbrw2r2qvwxy"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-block-buffer-0.7
  (package
    (name "rust-block-buffer")
    (version "0.7.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "block-buffer" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "12v8wizynqin0hqf140kmp9s38q223mp1b0hkqk8j5pk8720v560"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-generic-array" ,rust-generic-array-0.12)
         ("rust-byteorder" ,rust-byteorder-1)
         ("rust-byte-tools" ,rust-byte-tools-0.3)
         ("rust-block-padding" ,rust-block-padding-0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-block-buffer-0.9
  (package
    (name "rust-block-buffer")
    (version "0.9.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "block-buffer" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1r4pf90s7d7lj1wdjhlnqa26vvbm6pnc33z138lxpnp9srpi2lj1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-generic-array" ,rust-generic-array-0.14))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-block-padding-0.1
  (package
    (name "rust-block-padding")
    (version "0.1.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "block-padding" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1xbkmysiz23vimd17rnsjpw9bgjxipwfslwyygqlkx4in3dxwygs"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-byte-tools" ,rust-byte-tools-0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-blocking-1
  (package
    (name "rust-blocking")
    (version "1.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "blocking" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1k6ci9rflwr14izydrmvnfm0zab9xmyl714qns3a0y498rfvdk66"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-once-cell" ,rust-once-cell-1)
         ("rust-futures-lite" ,rust-futures-lite-1)
         ("rust-fastrand" ,rust-fastrand-1)
         ("rust-atomic-waker" ,rust-atomic-waker-1)
         ("rust-async-task" ,rust-async-task-4)
         ("rust-async-channel" ,rust-async-channel-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-boxfnonce-0.1
  (package
    (name "rust-boxfnonce")
    (version "0.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "boxfnonce" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "09ilf4zyx92hyhkxlsxksfyprzr9iwq5gqqb22aaqr32c8fwp22r"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-bstr-1
  (package
    (name "rust-bstr")
    (version "1.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bstr" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1xafaq57fzwa38yjwdzvqlbm8h207sjm585y4kdxjv1znj5ycim2"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde" ,rust-serde-1)
         ("rust-memchr" ,rust-memchr-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-bumpalo-3
  (package
    (name "rust-bumpalo")
    (version "3.10.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bumpalo" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1qrx6sg13yxljk1yr705j5wg34iiy3531by1hqrpiihl8qhvvk1p"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-byte-tools-0.3
  (package
    (name "rust-byte-tools")
    (version "0.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "byte-tools" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1mqi29wsm8njpl51pfwr31wmpzs5ahlcb40wsjyd92l90ixcmdg3"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-bytecheck-0.6
  (package
    (name "rust-bytecheck")
    (version "0.6.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bytecheck" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0vs0a8p3bpaz3vc15zknqkd5ajgzgswf2bmd1mbwdbdm28naq76i"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-ptr-meta" ,rust-ptr-meta-0.1)
         ("rust-bytecheck-derive"
          ,rust-bytecheck-derive-0.6))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-bytecheck-derive-0.6
  (package
    (name "rust-bytecheck-derive")
    (version "0.6.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bytecheck_derive" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1gxr63mi91rrjzfzcb8pfwsnarp9i2w1n168nc05aq4fx7mpdr8k"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-syn" ,rust-syn-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-byteorder-1
  (package
    (name "rust-byteorder")
    (version "1.4.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "byteorder" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0456lv9xi1a5bcm32arknf33ikv76p3fr9yzki4lb2897p2qkh8l"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-bytes-0.5
  (package
    (name "rust-bytes")
    (version "0.5.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bytes" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0f5s7xq6qzmdh22ygsy8v0sp02m51y0radvq4i4y8cizy1lfqk0f"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-bytes-1
  (package
    (name "rust-bytes")
    (version "1.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bytes" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1gkh3fk4fm9xv5znlib723h5md5sxsvbd5113sbxff6g1lmgvcl9"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-cache-padded-1
  (package
    (name "rust-cache-padded")
    (version "1.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cache-padded" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0b39fmvn6j47xcyc03biyh8kdd52qwhb55xmx72hj3y73ri5kny1"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-cassowary-0.3
  (package
    (name "rust-cassowary")
    (version "0.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cassowary" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0lvanj0gsk6pc1chqrh4k5k0vi1rfbgzmsk46dwy3nmrqyw711nz"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-cc-1
  (package
    (name "rust-cc")
    (version "1.0.83")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cc" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1l643zidlb5iy1dskc5ggqs4wqa29a02f44piczqc8zcnsq4y5zi"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-cfg-if-0.1
  (package
    (name "rust-cfg-if")
    (version "0.1.10")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cfg-if" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "08h80ihs74jcyp24cd75wwabygbbdgl05k6p5dmq8akbr78vv1a7"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-cfg-if-1
  (package
    (name "rust-cfg-if")
    (version "1.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cfg-if" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1za0vb97n4brpzpv8lsbnzmq5r8f2b0cpqqr0sy8h5bn751xxwds"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-chrono-0.4
  (package
    (name "rust-chrono")
    (version "0.4.19")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "chrono" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0wyfl6c00vhfl562spnfcna3zkw8jqvcp652m9iskhl8j26dc2k7"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-winapi" ,rust-winapi-0.3)
         ("rust-time" ,rust-time-0.1)
         ("rust-num-traits" ,rust-num-traits-0.2)
         ("rust-num-integer" ,rust-num-integer-0.1)
         ("rust-libc" ,rust-libc-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-cipher-0.2
  (package
    (name "rust-cipher")
    (version "0.2.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cipher" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "00b8imbmdg7zdrbaczlivmdfdy09xldg95wl4iijl15xgjcfgy0j"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-generic-array" ,rust-generic-array-0.14))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-clap-3
  (package
    (name "rust-clap")
    (version "3.2.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "clap" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0yn861xzwxwsfdl3xa6pamvcqs8pm904z0ipg2hlbyxl74vxw83d"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-textwrap" ,rust-textwrap-0.15)
         ("rust-termcolor" ,rust-termcolor-1)
         ("rust-strsim" ,rust-strsim-0.10)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-indexmap" ,rust-indexmap-1)
         ("rust-clap-lex" ,rust-clap-lex-0.2)
         ("rust-clap-derive" ,rust-clap-derive-3)
         ("rust-bitflags" ,rust-bitflags-1)
         ("rust-atty" ,rust-atty-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-clap-complete-3
  (package
    (name "rust-clap-complete")
    (version "3.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "clap_complete" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0h6z59gxzkyyrz15f52kwsb4n82mlwqcn1zs5lqz1r15bymvlvhg"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-clap-derive-3
  (package
    (name "rust-clap-derive")
    (version "3.2.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "clap_derive" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1xwqpyjqqffdyy5ddbdziij6j3pgg29yq0k0hcrd7ywzp04aysq2"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-syn" ,rust-syn-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-proc-macro-error"
          ,rust-proc-macro-error-1)
         ("rust-heck" ,rust-heck-0.4))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-clap-lex-0.2
  (package
    (name "rust-clap-lex")
    (version "0.2.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "clap_lex" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "04rn568mpbc4smk1wlsh9ky4z12b53rgiv6g6i1fpssh0ikcsf2m"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-os-str-bytes" ,rust-os-str-bytes-6))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-close-fds-0.3
  (package
    (name "rust-close-fds")
    (version "0.3.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "close_fds" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1vd2i1gkvfcgdlzgrkgivhx3ky0zs98g8q3mwmwrxmg97pridi1v"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2)
         ("rust-cfg-if" ,rust-cfg-if-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-colored-2
  (package
    (name "rust-colored")
    (version "2.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "colored" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1gbcijscmznzy42rn213yp9ima7210zakgaqibgg1n441dsnyqdk"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-winapi" ,rust-winapi-0.3)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-atty" ,rust-atty-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-colorsys-0.6
  (package
    (name "rust-colorsys")
    (version "0.6.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "colorsys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0w5vvcq3497m184a399x2rh9fgmarwzdkjc87zzmasslklbzkzad"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-common-path-1
  (package
    (name "rust-common-path")
    (version "1.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "common-path" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "00firjly5xpb5hhmivnnhwakr1cwbqv8ckzyj0vbxczl89czg0i3"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-compact-bar-0.1
  (package
    (name "rust-compact-bar")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "compact-bar" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0000000000000000000000000000000000000000000000000000"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-zellij-tile-utils"
          ,rust-zellij-tile-utils-0.40)
         ("rust-zellij-tile" ,rust-zellij-tile-0.40)
         ("rust-unicode-width" ,rust-unicode-width-0.1)
         ("rust-colored" ,rust-colored-2)
         ("rust-ansi-term" ,rust-ansi-term-0.12))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-concurrent-queue-1
  (package
    (name "rust-concurrent-queue")
    (version "1.2.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "concurrent-queue" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "18w6hblcjjk9d0my3657ra1zdj79gwfjmzvc0b3985g01dahgv9h"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cache-padded" ,rust-cache-padded-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-concurrent-queue-2
  (package
    (name "rust-concurrent-queue")
    (version "2.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "concurrent-queue" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0z0bnpgcblhrms6gph7x78yplj3qmlr5mvl38v9641zsxiqngv32"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-crossbeam-utils"
          ,rust-crossbeam-utils-0.8))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-console-0.15
  (package
    (name "rust-console")
    (version "0.15.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "console" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0c9wif28i3q231gvjprqdq1glmgmmcdxpmxcwk1p0jx45k9k52x2"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-winapi" ,rust-winapi-0.3)
         ("rust-unicode-width" ,rust-unicode-width-0.1)
         ("rust-terminal-size" ,rust-terminal-size-0.1)
         ("rust-regex" ,rust-regex-1)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-encode-unicode" ,rust-encode-unicode-0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-const-fn-0.4
  (package
    (name "rust-const-fn")
    (version "0.4.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "const_fn" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0df9fv9jhnh9b4ni3s2fbfcvq77iia4lbb89fklwawbgv2vdrp7v"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-cookie-0.14
  (package
    (name "rust-cookie")
    (version "0.14.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cookie" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0ldryjb41r8n0ar2pya0bajlxr8s4j59fjkmyi5ppg1932rdg983"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-version-check" ,rust-version-check-0.9)
         ("rust-time" ,rust-time-0.2)
         ("rust-sha2" ,rust-sha2-0.9)
         ("rust-rand" ,rust-rand-0.8)
         ("rust-percent-encoding"
          ,rust-percent-encoding-2)
         ("rust-hmac" ,rust-hmac-0.10)
         ("rust-hkdf" ,rust-hkdf-0.10)
         ("rust-base64" ,rust-base64-0.13)
         ("rust-aes-gcm" ,rust-aes-gcm-0.8))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-core-foundation-sys-0.8
  (package
    (name "rust-core-foundation-sys")
    (version "0.8.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "core-foundation-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1p5r2wckarkpkyc4z83q08dwpvcafrb1h6fxfa3qnikh8szww9sq"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-corosensei-0.1
  (package
    (name "rust-corosensei")
    (version "0.1.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "corosensei" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1kgj9k74asnrdsgmdc4p1c4172vr8br3zhlbsv5hs2x5687zjiwq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-windows-sys" ,rust-windows-sys-0.33)
         ("rust-scopeguard" ,rust-scopeguard-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-autocfg" ,rust-autocfg-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-cpufeatures-0.2
  (package
    (name "rust-cpufeatures")
    (version "0.2.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cpufeatures" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0jsyrw41g5sqdpc9jgk57969hc0xw4c52j9amvmll4mbcwb019jr"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-cpuid-bool-0.2
  (package
    (name "rust-cpuid-bool")
    (version "0.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cpuid-bool" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1fpzag3g655p1lr08jgf5n89snjc2ycqx30mm0w3irc9fc3mvcnw"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-cranelift-bforest-0.86
  (package
    (name "rust-cranelift-bforest")
    (version "0.86.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cranelift-bforest" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0m8gdr2hgl1w13cd05dlb1jxkwzdvxr1d552vc7cd6i4wb6gm7sj"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cranelift-entity"
          ,rust-cranelift-entity-0.86))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-cranelift-codegen-0.86
  (package
    (name "rust-cranelift-codegen")
    (version "0.86.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cranelift-codegen" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "14jagk9ghlambscb90475n4jgqnjvrznl0wdbysqrz3yc5gi0za2"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-target-lexicon" ,rust-target-lexicon-0.12)
         ("rust-smallvec" ,rust-smallvec-1)
         ("rust-regalloc2" ,rust-regalloc2-0.3)
         ("rust-log" ,rust-log-0.4)
         ("rust-gimli" ,rust-gimli-0.26)
         ("rust-cranelift-isle" ,rust-cranelift-isle-0.86)
         ("rust-cranelift-entity"
          ,rust-cranelift-entity-0.86)
         ("rust-cranelift-codegen-shared"
          ,rust-cranelift-codegen-shared-0.86)
         ("rust-cranelift-codegen-meta"
          ,rust-cranelift-codegen-meta-0.86)
         ("rust-cranelift-bforest"
          ,rust-cranelift-bforest-0.86))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-cranelift-codegen-meta-0.86
  (package
    (name "rust-cranelift-codegen-meta")
    (version "0.86.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cranelift-codegen-meta" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1ldprm0lp9dr0i11h26wf067k9ps1x7snglf6m2qv0svv2z785jm"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cranelift-codegen-shared"
          ,rust-cranelift-codegen-shared-0.86))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-cranelift-codegen-shared-0.86
  (package
    (name "rust-cranelift-codegen-shared")
    (version "0.86.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cranelift-codegen-shared" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0rjqxh7z90k0msyf6isyw50l4xbi6nil829njns3x329ayp66fib"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-cranelift-entity-0.86
  (package
    (name "rust-cranelift-entity")
    (version "0.86.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cranelift-entity" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0viygzb8hfjra5pn9lqlbc5jcj0jlsgp7l539v4w2b674jk8mahi"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-cranelift-frontend-0.86
  (package
    (name "rust-cranelift-frontend")
    (version "0.86.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cranelift-frontend" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1l0vsb3mcwsj6m7hjq85c9nqa4samjp7lvnllsd9qmf9s7sfhkjl"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-target-lexicon" ,rust-target-lexicon-0.12)
         ("rust-smallvec" ,rust-smallvec-1)
         ("rust-log" ,rust-log-0.4)
         ("rust-cranelift-codegen"
          ,rust-cranelift-codegen-0.86))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-cranelift-isle-0.86
  (package
    (name "rust-cranelift-isle")
    (version "0.86.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cranelift-isle" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1b8a2x1l619wci0j2c6c7qw1nycnfc57sm9wgv1vhafrcd1v25pd"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-crossbeam-0.8
  (package
    (name "rust-crossbeam")
    (version "0.8.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "crossbeam" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0ibq943sxbli0d03aihys3n4l9apy88d22z9759b0g1wdf7miraa"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-crossbeam-utils"
          ,rust-crossbeam-utils-0.8)
         ("rust-crossbeam-queue"
          ,rust-crossbeam-queue-0.3)
         ("rust-crossbeam-epoch"
          ,rust-crossbeam-epoch-0.9)
         ("rust-crossbeam-deque"
          ,rust-crossbeam-deque-0.8)
         ("rust-crossbeam-channel"
          ,rust-crossbeam-channel-0.5)
         ("rust-cfg-if" ,rust-cfg-if-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-crossbeam-channel-0.5
  (package
    (name "rust-crossbeam-channel")
    (version "0.5.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "crossbeam-channel" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0lvcpv6hg1g1r6aamiq9b4958p4hjy8dsqzrnmj6hp36zgappajs"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-crossbeam-utils"
          ,rust-crossbeam-utils-0.8)
         ("rust-cfg-if" ,rust-cfg-if-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-crossbeam-deque-0.8
  (package
    (name "rust-crossbeam-deque")
    (version "0.8.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "crossbeam-deque" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "07nypn86id2lf912ahiww1jvqp0zbk2xa25ra7vzplph375c0mb4"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-crossbeam-utils"
          ,rust-crossbeam-utils-0.8)
         ("rust-crossbeam-epoch"
          ,rust-crossbeam-epoch-0.9)
         ("rust-cfg-if" ,rust-cfg-if-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-crossbeam-epoch-0.9
  (package
    (name "rust-crossbeam-epoch")
    (version "0.9.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "crossbeam-epoch" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "137ydqj18qddmazgqd16kjn766byixinmavra1hs0src389wyi8i"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-scopeguard" ,rust-scopeguard-1)
         ("rust-memoffset" ,rust-memoffset-0.6)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-crossbeam-utils"
          ,rust-crossbeam-utils-0.8)
         ("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-autocfg" ,rust-autocfg-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-crossbeam-queue-0.3
  (package
    (name "rust-crossbeam-queue")
    (version "0.3.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "crossbeam-queue" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "18kpa83m1ivz3230f4ax95f9pgcclj227rg4y1w5fyja1x0dh98z"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-crossbeam-utils"
          ,rust-crossbeam-utils-0.8)
         ("rust-cfg-if" ,rust-cfg-if-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-crossbeam-utils-0.8
  (package
    (name "rust-crossbeam-utils")
    (version "0.8.15")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "crossbeam-utils" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0jwq8srmjcwvq9q883k9zyb26qqznaj4jjqdxmvw7xcmrkc3q1iw"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cfg-if" ,rust-cfg-if-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-crypto-mac-0.10
  (package
    (name "rust-crypto-mac")
    (version "0.10.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "crypto-mac" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "06h84hcaksgjzzzc9g9dpmifwx221qzzif6fw8l807khxh471w5z"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-subtle" ,rust-subtle-2)
         ("rust-generic-array" ,rust-generic-array-0.14))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-csscolorparser-0.6
  (package
    (name "rust-csscolorparser")
    (version "0.6.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "csscolorparser" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1gxh11hajx96mf5sd0az6mfsxdryfqvcfcphny3yfbfscqq7sapb"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-phf" ,rust-phf-0.11)
         ("rust-lab" ,rust-lab-0.11))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-ctor-0.1
  (package
    (name "rust-ctor")
    (version "0.1.22")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ctor" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1712zdlrmcfkfw38dkj67xg72fd0p9slyqqi64c6n94zgi7vwxzq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-syn" ,rust-syn-1)
         ("rust-quote" ,rust-quote-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-ctr-0.6
  (package
    (name "rust-ctr")
    (version "0.6.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ctr" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0zvyf13675hrlc37myj97k5ng7m1mj3d9p4ic4yvyhvl9zak0jpv"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cipher" ,rust-cipher-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-curl-0.4
  (package
    (name "rust-curl")
    (version "0.4.44")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "curl" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "08hsq6ssy228df56adv2wbgam05f5rw1f2wzs7mhkb678qbx36sh"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-winapi" ,rust-winapi-0.3)
         ("rust-socket2" ,rust-socket2-0.4)
         ("rust-schannel" ,rust-schannel-0.1)
         ("rust-openssl-sys" ,rust-openssl-sys-0.9)
         ("rust-openssl-probe" ,rust-openssl-probe-0.1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-curl-sys" ,rust-curl-sys-0.4))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-curl-sys-0.4
  (package
    (name "rust-curl-sys")
    (version "0.4.68+curl-8.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "curl-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0bvmslw0g3hhdgws8jhxzj7jamsy5f1kq9xj2r5kf3ini26x385l"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-windows-sys" ,rust-windows-sys-0.48)
         ("rust-vcpkg" ,rust-vcpkg-0.2)
         ("rust-pkg-config" ,rust-pkg-config-0.3)
         ("rust-openssl-sys" ,rust-openssl-sys-0.9)
         ("rust-libz-sys" ,rust-libz-sys-1)
         ("rust-libnghttp2-sys" ,rust-libnghttp2-sys-0.1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-cc" ,rust-cc-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-daemonize-0.4
  (package
    (name "rust-daemonize")
    (version "0.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "daemonize" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "05cqr2zjxrxyg23snykd03sgqwxn0pvwj2lzh50bclsgwc9lbhkh"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2)
         ("rust-boxfnonce" ,rust-boxfnonce-0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-darling-0.13
  (package
    (name "rust-darling")
    (version "0.13.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "darling" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0g25pad4mhq7315mw9n4wpg8j3mwyhwvr541kgdl0aar1j2ra7d0"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-darling-macro" ,rust-darling-macro-0.13)
         ("rust-darling-core" ,rust-darling-core-0.13))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-darling-core-0.13
  (package
    (name "rust-darling-core")
    (version "0.13.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "darling_core" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "046n83f9jpszlngpjxkqi39ayzxf5a35q673c69jr1dn0ylnb7c5"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-syn" ,rust-syn-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-ident-case" ,rust-ident-case-1)
         ("rust-fnv" ,rust-fnv-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-darling-macro-0.13
  (package
    (name "rust-darling-macro")
    (version "0.13.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "darling_macro" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0d8q8ibmsb1yzby6vwgh2wx892jqqfv9clwhpm19rprvz1wjd5ww"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-syn" ,rust-syn-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-darling-core" ,rust-darling-core-0.13))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-deltae-0.3
  (package
    (name "rust-deltae")
    (version "0.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "deltae" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0nmd9v0cfxz3rj9n7wv91pk3s7p2mc5w8l7a773zqqpclj8ws4p4"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-derivative-2
  (package
    (name "rust-derivative")
    (version "2.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "derivative" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "02vpb81wisk2zh1d5f44szzxamzinqgq2k8ydrfjj2wwkrgdvhzw"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-syn" ,rust-syn-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-destructure-traitobject-0.2
  (package
    (name "rust-destructure-traitobject")
    (version "0.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "destructure_traitobject" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1ir5x9f5zksr1fs788jy5g2hyyc2hnnx7kwi87wd451wd5apb1rw"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-dialoguer-0.10
  (package
    (name "rust-dialoguer")
    (version "0.10.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "dialoguer" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "11rgzrhi677w9gf1r3ip2x361svdkjkr2m5dsfca9fcljacg5ijr"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-zeroize" ,rust-zeroize-1)
         ("rust-tempfile" ,rust-tempfile-3)
         ("rust-shell-words" ,rust-shell-words-1)
         ("rust-console" ,rust-console-0.15))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-digest-0.8
  (package
    (name "rust-digest")
    (version "0.8.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "digest" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1madjl27f3kj5ql7kwgvb9c8b7yb7bv7yfgx7rqzj4i3fp4cil7k"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-generic-array" ,rust-generic-array-0.12))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-digest-0.9
  (package
    (name "rust-digest")
    (version "0.9.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "digest" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0rmhvk33rgvd6ll71z8sng91a52rw14p0drjn1da0mqa138n1pfk"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-generic-array" ,rust-generic-array-0.14))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-directories-5
  (package
    (name "rust-directories")
    (version "5.0.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "directories" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0dba6xzk79s1clqzxh2qlgzk3lmvvks1lzzjhhi3hd70hhxifjcs"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-dirs-sys" ,rust-dirs-sys-0.4))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-dirs-2
  (package
    (name "rust-dirs")
    (version "2.0.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "dirs" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1qymhyq7w7wlf1dirq6gsnabdyzg6yi2yyxkx6c4ldlkbjdaibhk"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-dirs-sys" ,rust-dirs-sys-0.3)
         ("rust-cfg-if" ,rust-cfg-if-0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-dirs-4
  (package
    (name "rust-dirs")
    (version "4.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "dirs" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0n8020zl4f0frfnzvgb9agvk4a14i1kjz4daqnxkgslndwmaffna"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-dirs-sys" ,rust-dirs-sys-0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-dirs-sys-0.3
  (package
    (name "rust-dirs-sys")
    (version "0.3.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "dirs-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "19md1cnkazham8a6kh22v12d8hh3raqahfk6yb043vrjr68is78v"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-winapi" ,rust-winapi-0.3)
         ("rust-redox-users" ,rust-redox-users-0.4)
         ("rust-libc" ,rust-libc-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-dirs-sys-0.4
  (package
    (name "rust-dirs-sys")
    (version "0.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "dirs-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "071jy0pvaad9lsa6mzawxrh7cmr7hsmsdxwzm7jzldfkrfjha3sj"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-windows-sys" ,rust-windows-sys-0.48)
         ("rust-redox-users" ,rust-redox-users-0.4)
         ("rust-option-ext" ,rust-option-ext-0.2)
         ("rust-libc" ,rust-libc-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-discard-1
  (package
    (name "rust-discard")
    (version "1.0.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "discard" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1h67ni5bxvg95s91wgicily4ix7lcw7cq0a5gy9njrybaibhyb91"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-dissimilar-1
  (package
    (name "rust-dissimilar")
    (version "1.0.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "dissimilar" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0phdkg9d9c1vmn5v74lq7rmaba2m52fkwcryd3cbw46pww5cc3i1"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-dynasm-1
  (package
    (name "rust-dynasm")
    (version "1.2.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "dynasm" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "12yf5l193j318bv9fxqw1r5j210mdzh0jgrna304wlkvh01a3ndd"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-syn" ,rust-syn-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-proc-macro-error"
          ,rust-proc-macro-error-1)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-byteorder" ,rust-byteorder-1)
         ("rust-bitflags" ,rust-bitflags-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-dynasmrt-1
  (package
    (name "rust-dynasmrt")
    (version "1.2.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "dynasmrt" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1aa1av01h0l8ms9fk32ydahby77fd3hhv85zsk51fsnp5fjabyv4"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-memmap2" ,rust-memmap2-0.5)
         ("rust-dynasm" ,rust-dynasm-1)
         ("rust-byteorder" ,rust-byteorder-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-either-1
  (package
    (name "rust-either")
    (version "1.6.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "either" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0mwl9vngqf5jvrhmhn9x60kr5hivxyjxbmby2pybncxfqhf4z3g7"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-encode-unicode-0.3
  (package
    (name "rust-encode-unicode")
    (version "0.3.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "encode_unicode" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "07w3vzrhxh9lpjgsg2y5bwzfar2aq35mdznvcp3zjl0ssj7d4mx3"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-enum-iterator-0.7
  (package
    (name "rust-enum-iterator")
    (version "0.7.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "enum-iterator" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1rldnx6avpz39i1bwb65d4gs303p40syyfc4zqwlx7mpxp2wbsjf"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-enum-iterator-derive"
          ,rust-enum-iterator-derive-0.7))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-enum-iterator-derive-0.7
  (package
    (name "rust-enum-iterator-derive")
    (version "0.7.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "enum-iterator-derive" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0ndihb41kmi6pxc2bs097abxliw2pgnnw412lhdqfymjc1vw6d61"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-syn" ,rust-syn-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-enumset-1
  (package
    (name "rust-enumset")
    (version "1.0.11")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "enumset" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0x70xzxs2sj0yn4sh99bwk8s51aswvyvf1ldm6kziwa89nrcv6a7"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-enumset-derive" ,rust-enumset-derive-0.6))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-enumset-derive-0.6
  (package
    (name "rust-enumset-derive")
    (version "0.6.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "enumset_derive" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0k1hhph1kjkw1p726fk9f4nxz3kgngm1grxypk7rr68xvkxs70za"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-syn" ,rust-syn-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-darling" ,rust-darling-0.13))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-erased-serde-0.3
  (package
    (name "rust-erased-serde")
    (version "0.3.20")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "erased-serde" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1zbzqr50vj05q0gphygrwls4pldaj4qwp1kxils4ddfhs3c2s4xd"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde" ,rust-serde-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-errno-0.3
  (package
    (name "rust-errno")
    (version "0.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "errno" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0fp7qy6fwagrnmi45msqnl01vksqwdb2qbbv60n9cz7rf0xfrksb"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-windows-sys" ,rust-windows-sys-0.48)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-errno-dragonfly"
          ,rust-errno-dragonfly-0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-errno-dragonfly-0.1
  (package
    (name "rust-errno-dragonfly")
    (version "0.1.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "errno-dragonfly" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1grrmcm6q8512hkq5yzch3yv8wafflc2apbmsaabiyk44yqz2s5a"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2)
         ("rust-cc" ,rust-cc-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-event-listener-2
  (package
    (name "rust-event-listener")
    (version "2.5.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "event-listener" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0wgbyhw4piqrrln6s61y3sc06wd3liwzzz18hazji3wk2ya31wvp"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-expect-test-1
  (package
    (name "rust-expect-test")
    (version "1.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "expect-test" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1lzqx5hqh1g4llzqby4z1d18xmrjjx63c5l0na7ycf6mmpzfmn9h"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-once-cell" ,rust-once-cell-1)
         ("rust-dissimilar" ,rust-dissimilar-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-fake-simd-0.1
  (package
    (name "rust-fake-simd")
    (version "0.1.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "fake-simd" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1vfylvk4va2ivqx85603lyqqp0zk52cgbs4n5nfbbbqx577qm2p8"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-fallible-iterator-0.2
  (package
    (name "rust-fallible-iterator")
    (version "0.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "fallible-iterator" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1xq759lsr8gqss7hva42azn3whgrbrs2sd9xpn92c5ickxm1fhs4"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-fastrand-1
  (package
    (name "rust-fastrand")
    (version "1.7.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "fastrand" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1pvci54f2cm69ybc308z213xdybgqpvf2pcvq1kch69mwp7g1z63"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-instant" ,rust-instant-0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-file-id-0.1
  (package
    (name "rust-file-id")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "file-id" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1hx8zmiqpydj4b471nd1llj1jb8bmjxbwqmq1jy92bm8dhgfffz1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-winapi-util" ,rust-winapi-util-0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-filedescriptor-0.8
  (package
    (name "rust-filedescriptor")
    (version "0.8.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "filedescriptor" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0vplyh0cw35kzq7smmp2ablq0zsknk5rkvvrywqsqfrchmjxk6bi"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-winapi" ,rust-winapi-0.3)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-libc" ,rust-libc-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-filetime-0.2
  (package
    (name "rust-filetime")
    (version "0.2.21")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "filetime" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0lsii3b9w4ya0g5nf9ycnif4izy8i492x5ri752d9sdfxi689g2w"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-windows-sys" ,rust-windows-sys-0.48)
         ("rust-redox-syscall" ,rust-redox-syscall-0.2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-cfg-if" ,rust-cfg-if-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-finl-unicode-1
  (package
    (name "rust-finl-unicode")
    (version "1.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "finl_unicode" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1ipdx778849czik798sjbgk5yhwxqybydac18d2g9jb20dxdrkwg"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-fixedbitset-0.4
  (package
    (name "rust-fixedbitset")
    (version "0.4.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "fixedbitset" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "101v41amgv5n9h4hcghvrbfk5vrncx1jwm35rn5szv4rk55i7rqc"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-fixture-plugin-for-tests-0.1
  (package
    (name "rust-fixture-plugin-for-tests")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "fixture-plugin-for-tests" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0000000000000000000000000000000000000000000000000000"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-zellij-tile" ,rust-zellij-tile-0.40)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-serde" ,rust-serde-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-flume-0.9
  (package
    (name "rust-flume")
    (version "0.9.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "flume" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0ck1w9881848xjjk93nxqsvnxfp4xsaysxxn23a210bg2amsvsqv"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-spinning-top" ,rust-spinning-top-0.2)
         ("rust-futures-sink" ,rust-futures-sink-0.3)
         ("rust-futures-core" ,rust-futures-core-0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-fnv-1
  (package
    (name "rust-fnv")
    (version "1.0.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "fnv" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1hc2mcqha06aibcaza94vbi81j6pr9a1bbxrxjfhc91zin8yr7iz"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-form-urlencoded-1
  (package
    (name "rust-form-urlencoded")
    (version "1.0.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "form_urlencoded" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1491fmakavcmsjbm3q6iy0bhmn9l422jasdhzx5hkljgza3mmhjz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-percent-encoding"
          ,rust-percent-encoding-2)
         ("rust-matches" ,rust-matches-0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-fsevent-sys-4
  (package
    (name "rust-fsevent-sys")
    (version "4.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "fsevent-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1liz67v8b0gcs8r31vxkvm2jzgl9p14i78yfqx81c8sdv817mvkn"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-futures-0.3
  (package
    (name "rust-futures")
    (version "0.3.28")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "futures" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0h7c1xvxk751c7xlnph6fh3rb77z4lig4qif7f8q79db2az2ld13"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-futures-task" ,rust-futures-task-0.3)
         ("rust-futures-sink" ,rust-futures-sink-0.3)
         ("rust-futures-io" ,rust-futures-io-0.3)
         ("rust-futures-executor"
          ,rust-futures-executor-0.3)
         ("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-futures-channel"
          ,rust-futures-channel-0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-futures-channel-0.3
  (package
    (name "rust-futures-channel")
    (version "0.3.28")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "futures-channel" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1wmm9wm5zjigxz61qkscmxp7c30zp08dy63spjz5pch9gva1hmcm"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-futures-sink" ,rust-futures-sink-0.3)
         ("rust-futures-core" ,rust-futures-core-0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-futures-core-0.3
  (package
    (name "rust-futures-core")
    (version "0.3.28")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "futures-core" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "137fdxy5amg9zkpa1kqnj7bnha6b94fmddz59w973x96gqxmijjb"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-futures-executor-0.3
  (package
    (name "rust-futures-executor")
    (version "0.3.28")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "futures-executor" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1q468di96knnla72xdvswic1ir2qkrf5czsdigc5n4l86a1fxv6c"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-futures-task" ,rust-futures-task-0.3)
         ("rust-futures-core" ,rust-futures-core-0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-futures-io-0.3
  (package
    (name "rust-futures-io")
    (version "0.3.28")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "futures-io" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0r4rhkdhq1my4fahlhz59barqa511bylq813w3w4gvbidq4p9zsg"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-futures-lite-1
  (package
    (name "rust-futures-lite")
    (version "1.13.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "futures-lite" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1kkbqhaib68nzmys2dc8j9fl2bwzf2s91jfk13lb2q3nwhfdbaa9"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-waker-fn" ,rust-waker-fn-1)
         ("rust-pin-project-lite"
          ,rust-pin-project-lite-0.2)
         ("rust-parking" ,rust-parking-2)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-futures-io" ,rust-futures-io-0.3)
         ("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-fastrand" ,rust-fastrand-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-futures-macro-0.3
  (package
    (name "rust-futures-macro")
    (version "0.3.28")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "futures-macro" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0wpfsqxwqk5k569xl0jzz4zxy85x695mndf7y9jn66q6jid59jl9"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-syn" ,rust-syn-2)
         ("rust-quote" ,rust-quote-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-futures-sink-0.3
  (package
    (name "rust-futures-sink")
    (version "0.3.28")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "futures-sink" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0vkv4frf4c6gm1ag9imjz8d0xvpnn22lkylsls0rffx147zf8fzl"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-futures-task-0.3
  (package
    (name "rust-futures-task")
    (version "0.3.28")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "futures-task" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0ravgihyarbplj32zp60asirfnaalw2wfsa0afhnl3kcpqrd3lvn"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-futures-util-0.3
  (package
    (name "rust-futures-util")
    (version "0.3.28")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "futures-util" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0cwmls9369w6q6hwlbm10q0plr6hmg8w28fpqvv4rmbjnx01xc16"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-slab" ,rust-slab-0.4)
         ("rust-pin-utils" ,rust-pin-utils-0.1)
         ("rust-pin-project-lite"
          ,rust-pin-project-lite-0.2)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-futures-task" ,rust-futures-task-0.3)
         ("rust-futures-sink" ,rust-futures-sink-0.3)
         ("rust-futures-macro" ,rust-futures-macro-0.3)
         ("rust-futures-io" ,rust-futures-io-0.3)
         ("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-futures-channel"
          ,rust-futures-channel-0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-fuzzy-matcher-0.3
  (package
    (name "rust-fuzzy-matcher")
    (version "0.3.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "fuzzy-matcher" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "153csv8rsk2vxagb68kpmiknvdd3bzqj03x805khckck28rllqal"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-thread-local" ,rust-thread-local-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-fxhash-0.2
  (package
    (name "rust-fxhash")
    (version "0.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "fxhash" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "037mb9ichariqi45xm6mz0b11pa92gj38ba0409z3iz239sns6y3"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-byteorder" ,rust-byteorder-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-generational-arena-0.2
  (package
    (name "rust-generational-arena")
    (version "0.2.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "generational-arena" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "00gn1g6nlky883qkacvsbp19yzl5ay8avq6f902jvxkl2mvkn7cf"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cfg-if" ,rust-cfg-if-0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-generic-array-0.12
  (package
    (name "rust-generic-array")
    (version "0.12.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "generic-array" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1gfpay78vijl9vrwl1k9v7fbvbhkhcmnrk4kfg9l6x24y4s9zpzz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-typenum" ,rust-typenum-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-generic-array-0.14
  (package
    (name "rust-generic-array")
    (version "0.14.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "generic-array" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "00qqhls43bzvyb7s26iw6knvsz3mckbxl3rhaahvypzhqwzd6j7x"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-version-check" ,rust-version-check-0.9)
         ("rust-typenum" ,rust-typenum-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-getopts-0.2
  (package
    (name "rust-getopts")
    (version "0.2.21")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "getopts" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1mgb3qvivi26gs6ihqqhh8iyhp3vgxri6vwyrwg28w0xqzavznql"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-unicode-width" ,rust-unicode-width-0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-getrandom-0.1
  (package
    (name "rust-getrandom")
    (version "0.1.16")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "getrandom" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1kjzmz60qx9mn615ks1akjbf36n3lkv27zfwbcam0fzmj56wphwg"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-wasi" ,rust-wasi-0.9)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-cfg-if" ,rust-cfg-if-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-getrandom-0.2
  (package
    (name "rust-getrandom")
    (version "0.2.10")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "getrandom" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "09zlimhhskzf7cmgcszix05wyz2i6fcpvh711cv1klsxl6r3chdy"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-wasi" ,rust-wasi-0.11)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-cfg-if" ,rust-cfg-if-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-ghash-0.3
  (package
    (name "rust-ghash")
    (version "0.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ghash" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0xd362xh17hadc2194dd6kjjq0ak1j4x7kkmfmpq9hw2s564wc4p"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-polyval" ,rust-polyval-0.4)
         ("rust-opaque-debug" ,rust-opaque-debug-0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-ghost-0.1
  (package
    (name "rust-ghost")
    (version "0.1.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ghost" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0jpr9df3xq5zvi20nkfyq8m63iwjlcgcrhqsdzgx73rynvzi7j3n"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-syn" ,rust-syn-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-gimli-0.26
  (package
    (name "rust-gimli")
    (version "0.26.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gimli" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1m0vi36ypv4gx9gzcw6y456yqnlypizhwlcqrmg6vkwd0lnkgk3q"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-stable-deref-trait"
          ,rust-stable-deref-trait-1)
         ("rust-indexmap" ,rust-indexmap-1)
         ("rust-fallible-iterator"
          ,rust-fallible-iterator-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-globset-0.4
  (package
    (name "rust-globset")
    (version "0.4.10")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "globset" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1z4slzdx80qmv5l7pv2ac82jihwm4rrz9i50w6fybzfyk9c79782"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-regex" ,rust-regex-1)
         ("rust-log" ,rust-log-0.4)
         ("rust-fnv" ,rust-fnv-1)
         ("rust-bstr" ,rust-bstr-1)
         ("rust-aho-corasick" ,rust-aho-corasick-0.7))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-gloo-timers-0.2
  (package
    (name "rust-gloo-timers")
    (version "0.2.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gloo-timers" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1yf8l55p6j5kzz100jgfjc3slb0bk8063jbyxsds5hlc3ind1dsz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2)
         ("rust-js-sys" ,rust-js-sys-0.3)
         ("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-futures-channel"
          ,rust-futures-channel-0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-hashbrown-0.11
  (package
    (name "rust-hashbrown")
    (version "0.11.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "hashbrown" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0vkjsf5nzs7qcia5ya79j9sq2p1caz4crrncr1675wwyj3ag0pmb"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-hashbrown-0.12
  (package
    (name "rust-hashbrown")
    (version "0.12.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "hashbrown" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1268ka4750pyg2pbgsr43f0289l5zah4arir2k4igx5a8c6fg7la"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-ahash" ,rust-ahash-0.7))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-heck-0.3
  (package
    (name "rust-heck")
    (version "0.3.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "heck" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0b0kkr790p66lvzn9nsmfjvydrbmh9z5gb664jchwgw64vxiwqkd"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-unicode-segmentation"
          ,rust-unicode-segmentation-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-heck-0.4
  (package
    (name "rust-heck")
    (version "0.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "heck" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1ygphsnfwl2xpa211vbqkz1db6ri1kvkg8p8sqybi37wclg7fh15"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-hermit-abi-0.1
  (package
    (name "rust-hermit-abi")
    (version "0.1.19")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "hermit-abi" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0cxcm8093nf5fyn114w8vxbrbcyvv91d4015rdnlgfll7cs6gd32"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-hermit-abi-0.3
  (package
    (name "rust-hermit-abi")
    (version "0.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "hermit-abi" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "11j2v3q58kmi5mhjvh6hfrb7il2yzg7gmdf5lpwnwwv6qj04im7y"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-hex-0.4
  (package
    (name "rust-hex")
    (version "0.4.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "hex" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0w1a4davm1lgzpamwnba907aysmlrnygbqmfis2mqjx5m552a93z"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-highway-0.6
  (package
    (name "rust-highway")
    (version "0.6.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "highway" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0p9r4ss563gj44qamnrr5bqf0p9lb8gjc0w5jqmk3jlmyrlbjq9l"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-hkdf-0.10
  (package
    (name "rust-hkdf")
    (version "0.10.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "hkdf" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0kwn3scjvv2x8zc6nz3wrnzxp9shpsdxnjqiyv2r65r3kiijzasi"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-hmac" ,rust-hmac-0.10)
         ("rust-digest" ,rust-digest-0.9))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-hmac-0.10
  (package
    (name "rust-hmac")
    (version "0.10.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "hmac" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "058yxq54x7xn0gk2vy9bl51r32c9z7qlcl2b80bjh3lk3rmiqi61"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-digest" ,rust-digest-0.9)
         ("rust-crypto-mac" ,rust-crypto-mac-0.10))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-http-0.2
  (package
    (name "rust-http")
    (version "0.2.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "http" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "10j4jjpngaymxjvi92hllr2y6acr09pq61cvzxd44qzvkb4zyvmx"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-itoa" ,rust-itoa-1)
         ("rust-fnv" ,rust-fnv-1)
         ("rust-bytes" ,rust-bytes-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-http-client-6
  (package
    (name "rust-http-client")
    (version "6.5.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "http-client" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "19g19jli98cd0ywrzcsbw5j34rypm8n43yszxa3gaaqyr46m2iqr"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-log" ,rust-log-0.4)
         ("rust-isahc" ,rust-isahc-0.9)
         ("rust-http-types" ,rust-http-types-2)
         ("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-async-trait" ,rust-async-trait-0.1)
         ("rust-async-std" ,rust-async-std-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-http-types-2
  (package
    (name "rust-http-types")
    (version "2.12.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "http-types" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1bgmfmvirsa1alcyw15mkh227j3a62aq1x47lkxxnfnnf9x1i6vf"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-url" ,rust-url-2)
         ("rust-serde-urlencoded"
          ,rust-serde-urlencoded-0.7)
         ("rust-serde-qs" ,rust-serde-qs-0.8)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-rand" ,rust-rand-0.7)
         ("rust-pin-project-lite"
          ,rust-pin-project-lite-0.2)
         ("rust-infer" ,rust-infer-0.2)
         ("rust-futures-lite" ,rust-futures-lite-1)
         ("rust-cookie" ,rust-cookie-0.14)
         ("rust-base64" ,rust-base64-0.13)
         ("rust-async-std" ,rust-async-std-1)
         ("rust-async-channel" ,rust-async-channel-1)
         ("rust-anyhow" ,rust-anyhow-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-humantime-2
  (package
    (name "rust-humantime")
    (version "2.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "humantime" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1r55pfkkf5v0ji1x6izrjwdq9v6sc7bv99xj6srywcar37xmnfls"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-id-arena-2
  (package
    (name "rust-id-arena")
    (version "2.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "id-arena" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "01ch8jhpgnih8sawqs44fqsqpc7bzwgy0xpi6j0f4j0i5mkvr8i5"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-ident-case-1
  (package
    (name "rust-ident-case")
    (version "1.0.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ident_case" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0fac21q6pwns8gh1hz3nbq15j8fi441ncl6w4vlnd1cmc55kiq5r"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-idna-0.2
  (package
    (name "rust-idna")
    (version "0.2.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "idna" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1y7ca2w5qp9msgl57n03zqp78gq1bk2crqzg6kv7a542mdphm2j1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-unicode-normalization"
          ,rust-unicode-normalization-0.1)
         ("rust-unicode-bidi" ,rust-unicode-bidi-0.3)
         ("rust-matches" ,rust-matches-0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-ignore-0.4
  (package
    (name "rust-ignore")
    (version "0.4.20")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ignore" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "14kl9fv83klbnjxdv0y8lpwlj3gkypxf3vbrmm29m2jkmcyqgryv"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-winapi-util" ,rust-winapi-util-0.1)
         ("rust-walkdir" ,rust-walkdir-2)
         ("rust-thread-local" ,rust-thread-local-1)
         ("rust-same-file" ,rust-same-file-1)
         ("rust-regex" ,rust-regex-1)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-log" ,rust-log-0.4)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-globset" ,rust-globset-0.4))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-include-dir-0.7
  (package
    (name "rust-include-dir")
    (version "0.7.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "include_dir" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "17pinxhivh3chkbjmbg9sl0x3h7wwry2zc2p12gfh8kizyp2yxhq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-include-dir-macros"
          ,rust-include-dir-macros-0.7))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-include-dir-macros-0.7
  (package
    (name "rust-include-dir-macros")
    (version "0.7.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "include_dir_macros" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0gsa6z58wxgw9j58w60wyjpk2nv3pd86kimw2akwyzpmbi5jhfdi"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-quote" ,rust-quote-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-indexmap-1
  (package
    (name "rust-indexmap")
    (version "1.8.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "indexmap" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0nnaw0whv3xysrpjrz69bsibbscd81rwx63s6f4kbajv1ia2s0g6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-hashbrown" ,rust-hashbrown-0.11)
         ("rust-autocfg" ,rust-autocfg-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-infer-0.2
  (package
    (name "rust-infer")
    (version "0.2.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "infer" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1b4ziqcv0d1wga5yfqf620dkgzijsdw3ylnzq61bfaxla2d85sb4"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-inotify-0.9
  (package
    (name "rust-inotify")
    (version "0.9.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "inotify" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1zxb04c4qccp8wnr3v04l503qpxzxzzzph61amlqbsslq4z9s1pq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2)
         ("rust-inotify-sys" ,rust-inotify-sys-0.1)
         ("rust-bitflags" ,rust-bitflags-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-inotify-sys-0.1
  (package
    (name "rust-inotify-sys")
    (version "0.1.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "inotify-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1syhjgvkram88my04kv03s0zwa66mdwa5v7ddja3pzwvx2sh4p70"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-insta-1
  (package
    (name "rust-insta")
    (version "1.14.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "insta" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "154f3ncpibbljkz4yvfqk5299asmdyybcpw01ijh2g64hzqn16b8"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-similar" ,rust-similar-2)
         ("rust-serde-yaml" ,rust-serde-yaml-0.8)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-console" ,rust-console-0.15))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-instant-0.1
  (package
    (name "rust-instant")
    (version "0.1.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "instant" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0b2bx5qdlwayriidhrag8vhy10kdfimfhmb3jnjmsz2h9j1bwnvs"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cfg-if" ,rust-cfg-if-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-interprocess-1
  (package
    (name "rust-interprocess")
    (version "1.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "interprocess" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1yrq3xmyf8c506z2fkiyqcxiqv21rap72fz6npizybz47czm7wl1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-winapi" ,rust-winapi-0.3)
         ("rust-to-method" ,rust-to-method-1)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-spinning" ,rust-spinning-0.1)
         ("rust-rustc-version" ,rust-rustc-version-0.4)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-intmap" ,rust-intmap-0.7)
         ("rust-futures-io" ,rust-futures-io-0.3)
         ("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-blocking" ,rust-blocking-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-intmap-0.7
  (package
    (name "rust-intmap")
    (version "0.7.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "intmap" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1ffph34qfda5zxdvy2pvjnip9hgzbjcxw53pvdpcjaxc8n7z4lmf"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-inventory-0.2
  (package
    (name "rust-inventory")
    (version "0.2.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "inventory" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "06h9xw67wx18rank4yyz93iq89j0fk6fbazryfvf5ach1dp4qd44"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-ghost" ,rust-ghost-0.1)
         ("rust-ctor" ,rust-ctor-0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-io-lifetimes-1
  (package
    (name "rust-io-lifetimes")
    (version "1.0.10")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "io-lifetimes" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "08625nsz0lgbd7c9lly6b6l45viqpsnj9jbsixd9mrz7596wfrlw"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-windows-sys" ,rust-windows-sys-0.48)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-hermit-abi" ,rust-hermit-abi-0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-is-terminal-0.4
  (package
    (name "rust-is-terminal")
    (version "0.4.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "is-terminal" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "07xyfla3f2jjb666s72la5jvl9zq7mixbqkjvyfi5j018rhr7kxd"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-windows-sys" ,rust-windows-sys-0.48)
         ("rust-rustix" ,rust-rustix-0.37)
         ("rust-io-lifetimes" ,rust-io-lifetimes-1)
         ("rust-hermit-abi" ,rust-hermit-abi-0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-is-ci-1
  (package
    (name "rust-is-ci")
    (version "1.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "is_ci" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1ywra2z56x6d4pc02zq24a4x7gvpixynh9524icbpchbf9ydwv31"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-isahc-0.9
  (package
    (name "rust-isahc")
    (version "0.9.14")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "isahc" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "12iqz5fj0509pr813pds2fgdk649a0b6ipvy3pqjwb1ywh68m572"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-waker-fn" ,rust-waker-fn-1)
         ("rust-url" ,rust-url-2)
         ("rust-tracing-futures"
          ,rust-tracing-futures-0.2)
         ("rust-tracing" ,rust-tracing-0.1)
         ("rust-sluice" ,rust-sluice-0.5)
         ("rust-slab" ,rust-slab-0.4)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-log" ,rust-log-0.4)
         ("rust-http" ,rust-http-0.2)
         ("rust-futures-lite" ,rust-futures-lite-1)
         ("rust-flume" ,rust-flume-0.9)
         ("rust-curl-sys" ,rust-curl-sys-0.4)
         ("rust-curl" ,rust-curl-0.4)
         ("rust-crossbeam-utils"
          ,rust-crossbeam-utils-0.8)
         ("rust-bytes" ,rust-bytes-0.5))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-itertools-0.10
  (package
    (name "rust-itertools")
    (version "0.10.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "itertools" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0ww45h7nxx5kj6z2y6chlskxd1igvs4j507anr6dzg99x1h25zdh"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-either" ,rust-either-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-itoa-1
  (package
    (name "rust-itoa")
    (version "1.0.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "itoa" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "13ap85z7slvma9c36bzi7h5j66dm5sxm4a2g7wiwxbsh826nfb0i"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-js-sys-0.3
  (package
    (name "rust-js-sys")
    (version "0.3.64")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "js-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0nlkiwpm8dyqcf1xyc6qmrankcgdd3fpzc0qyfq2sw3z97z9bwf5"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-kdl-4
  (package
    (name "rust-kdl")
    (version "4.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "kdl" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "19kljlwdhfyidz0g1vgwrrjsrd25rmplfknrpccdwbhf3qvqlf58"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-thiserror" ,rust-thiserror-1)
         ("rust-nom" ,rust-nom-7)
         ("rust-miette" ,rust-miette-5))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-kqueue-1
  (package
    (name "rust-kqueue")
    (version "1.0.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "kqueue" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "165dvjbf7s0nv6q8r9zkzi1v0g81c65a81wqm9bi5xavl45wd3rc"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2)
         ("rust-kqueue-sys" ,rust-kqueue-sys-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-kqueue-sys-1
  (package
    (name "rust-kqueue-sys")
    (version "1.0.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "kqueue-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "11z5labbms9vn9m6csi5383dmrlmdgsxq13ls9fwa6zhi5a5hrw3"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2)
         ("rust-bitflags" ,rust-bitflags-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-kv-log-macro-1
  (package
    (name "rust-kv-log-macro")
    (version "1.0.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "kv-log-macro" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0zwp4bxkkp87rl7xy2dain77z977rvcry1gmr5bssdbn541v7s0d"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-log" ,rust-log-0.4))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-lab-0.11
  (package
    (name "rust-lab")
    (version "0.11.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "lab" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "13ymsn5cwl5i9pmp5mfmbap7q688dcp9a17q82crkvb784yifdmz"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-lazy-static-1
  (package
    (name "rust-lazy-static")
    (version "1.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "lazy_static" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0in6ikhw8mgl33wjv6q6xfrb5b9jr16q8ygjy803fay4zcisvaz2"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-leb128-0.2
  (package
    (name "rust-leb128")
    (version "0.2.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "leb128" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0rxxjdn76sjbrb08s4bi7m4x47zg68f71jzgx8ww7j0cnivjckl8"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-lev-distance-0.1
  (package
    (name "rust-lev-distance")
    (version "0.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "lev_distance" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0pk26fp1fcjyg2ml8g5ma1jj2gvgnmmri4md8y3bqdjr46yx3nbj"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-libc-0.2
  (package
    (name "rust-libc")
    (version "0.2.149")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "libc" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "16z2zqswcbk1qg5yigfyr0d44v0974amdaj564dmv5dpi2y770d0"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-libnghttp2-sys-0.1
  (package
    (name "rust-libnghttp2-sys")
    (version "0.1.8+1.55.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "libnghttp2-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0h53v0jg0ihlqy6v7iz7rhrp70hbz9qxp5nfvaswvb9d35n9bbjg"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2)
         ("rust-cc" ,rust-cc-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-libssh2-sys-0.2
  (package
    (name "rust-libssh2-sys")
    (version "0.2.23")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "libssh2-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1jplndqhlsygjmsni1ydb4zbw0j5jjr47bmqnjkwif5qnipa755h"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-vcpkg" ,rust-vcpkg-0.2)
         ("rust-pkg-config" ,rust-pkg-config-0.3)
         ("rust-openssl-sys" ,rust-openssl-sys-0.9)
         ("rust-libz-sys" ,rust-libz-sys-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-cc" ,rust-cc-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-libz-sys-1
  (package
    (name "rust-libz-sys")
    (version "1.1.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "libz-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1gqb8nk7j4ngvlcll8plm2fvjwic40p2g4qp20pwry1m74f7c0lp"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-vcpkg" ,rust-vcpkg-0.2)
         ("rust-pkg-config" ,rust-pkg-config-0.3)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-cc" ,rust-cc-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-linked-hash-map-0.5
  (package
    (name "rust-linked-hash-map")
    (version "0.5.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "linked-hash-map" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1ww8zsraqnvrsknd315481185igwkx5n14xnhq5i8216z65b7fbz"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-linux-raw-sys-0.3
  (package
    (name "rust-linux-raw-sys")
    (version "0.3.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "linux-raw-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "17s7qr5h82blrxy29014zzhr30jcxcjc8r16v2p31rzcfal7xsgc"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-lock-api-0.4
  (package
    (name "rust-lock-api")
    (version "0.4.11")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "lock_api" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0iggx0h4jx63xm35861106af3jkxq06fpqhpkhgw0axi2n38y5iw"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-scopeguard" ,rust-scopeguard-1)
         ("rust-autocfg" ,rust-autocfg-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-log-0.4
  (package
    (name "rust-log")
    (version "0.4.17")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "log" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0biqlaaw1lsr8bpnmbcc0fvgjj34yy79ghqzyi0ali7vgil2xcdb"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-value-bag" ,rust-value-bag-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-cfg-if" ,rust-cfg-if-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-log-mdc-0.1
  (package
    (name "rust-log-mdc")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "log-mdc" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1iw1x3qhjvrac35spikn5h06a1rxd9vw216jk8h52jhz9i0j2kd9"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-log4rs-1
  (package
    (name "rust-log4rs")
    (version "1.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "log4rs" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1z9kfnba38smyrpmq49pjl82yqbvj2h81m3878cvhycydmwa2v6k"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-winapi" ,rust-winapi-0.3)
         ("rust-typemap-ors" ,rust-typemap-ors-1)
         ("rust-thread-id" ,rust-thread-id-4)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-serde-yaml" ,rust-serde-yaml-0.8)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-serde-value" ,rust-serde-value-0.7)
         ("rust-serde" ,rust-serde-1)
         ("rust-parking-lot" ,rust-parking-lot-0.12)
         ("rust-log-mdc" ,rust-log-mdc-0.1)
         ("rust-log" ,rust-log-0.4)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-humantime" ,rust-humantime-2)
         ("rust-fnv" ,rust-fnv-1)
         ("rust-derivative" ,rust-derivative-2)
         ("rust-chrono" ,rust-chrono-0.4)
         ("rust-arc-swap" ,rust-arc-swap-1)
         ("rust-anyhow" ,rust-anyhow-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-mach-0.3
  (package
    (name "rust-mach")
    (version "0.3.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "mach" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1yksa8lwzqh150gr4417rls1wk20asy9vhp8kq5g9n7z58xyh8xq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-maplit-1
  (package
    (name "rust-maplit")
    (version "1.0.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "maplit" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "07b5kjnhrrmfhgqm9wprjw8adx6i225lqp49gasgqg74lahnabiy"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-matches-0.1
  (package
    (name "rust-matches")
    (version "0.1.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "matches" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0gw5ib38jfgyyah8nyyxr036grqv1arkf1srgfa4h386dav7iqx3"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-memchr-2
  (package
    (name "rust-memchr")
    (version "2.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "memchr" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0vanfk5mzs1g1syqnj03q8n0syggnhn55dq535h2wxr7rwpfbzrd"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-memmap2-0.5
  (package
    (name "rust-memmap2")
    (version "0.5.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "memmap2" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "163lwxgmjdsnjdxjfd8vxj66ydxwp07himpar3pz4ymi8pribbwm"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-memmem-0.1
  (package
    (name "rust-memmem")
    (version "0.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "memmem" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "05ccifqgxdfxk6yls41ljabcccsz3jz6549l1h3cwi17kr494jm6"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-memoffset-0.6
  (package
    (name "rust-memoffset")
    (version "0.6.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "memoffset" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1kkrzll58a3ayn5zdyy9i1f1v3mx0xgl29x0chq614zazba638ss"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-autocfg" ,rust-autocfg-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-miette-5
  (package
    (name "rust-miette")
    (version "5.8.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "miette" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0mxkxawyqc6ybl3xp997kakwm89h5vw03rnqxylwlyam3n4r5acj"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-unicode-width" ,rust-unicode-width-0.1)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-textwrap" ,rust-textwrap-0.15)
         ("rust-terminal-size" ,rust-terminal-size-0.1)
         ("rust-supports-unicode"
          ,rust-supports-unicode-2)
         ("rust-supports-hyperlinks"
          ,rust-supports-hyperlinks-2)
         ("rust-supports-color" ,rust-supports-color-2)
         ("rust-owo-colors" ,rust-owo-colors-3)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-miette-derive" ,rust-miette-derive-5)
         ("rust-is-terminal" ,rust-is-terminal-0.4)
         ("rust-backtrace-ext" ,rust-backtrace-ext-0.2)
         ("rust-backtrace" ,rust-backtrace-0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-miette-derive-5
  (package
    (name "rust-miette-derive")
    (version "5.8.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "miette-derive" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0ls1hdsnx6afpzhknzmai73axcg127jvx54kd7kcr6va30jwcrac"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-syn" ,rust-syn-2)
         ("rust-quote" ,rust-quote-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-mime-0.3
  (package
    (name "rust-mime")
    (version "0.3.17")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "mime" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "16hkibgvb9klh0w0jk5crr5xv90l3wlf77ggymzjmvl1818vnxv8"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-mime-guess-2
  (package
    (name "rust-mime-guess")
    (version "2.0.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "mime_guess" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1vs28rxnbfwil6f48hh58lfcx90klcvg68gxdc60spwa4cy2d4j1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-unicase" ,rust-unicase-2)
         ("rust-mime" ,rust-mime-0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-minimal-lexical-0.2
  (package
    (name "rust-minimal-lexical")
    (version "0.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "minimal-lexical" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "16ppc5g84aijpri4jzv14rvcnslvlpphbszc7zzp6vfkddf4qdb8"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-miniz-oxide-0.5
  (package
    (name "rust-miniz-oxide")
    (version "0.5.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "miniz_oxide" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1k1wfxb35v129mhqy14yqhrj3wvknafrwygiq7zvi0m5iml7ap3g"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-adler" ,rust-adler-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-mio-0.7
  (package
    (name "rust-mio")
    (version "0.7.14")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "mio" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1k05cah6zdww6i2qc7gaxbbja4ppyjycipl2y0lhiiwpzq2b8rw0"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-winapi" ,rust-winapi-0.3)
         ("rust-ntapi" ,rust-ntapi-0.3)
         ("rust-miow" ,rust-miow-0.3)
         ("rust-log" ,rust-log-0.4)
         ("rust-libc" ,rust-libc-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-mio-0.8
  (package
    (name "rust-mio")
    (version "0.8.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "mio" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1ygx5chq81k3vk2bx722xwcwf2qydmm337jsnijgzd7mxx39m7av"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-windows-sys" ,rust-windows-sys-0.45)
         ("rust-wasi" ,rust-wasi-0.11)
         ("rust-log" ,rust-log-0.4)
         ("rust-libc" ,rust-libc-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-miow-0.3
  (package
    (name "rust-miow")
    (version "0.3.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "miow" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "08afp2xfpxmdw003111lxz6g9jgbj4zi2fpldvv7da6d4nqcbwdr"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-winapi" ,rust-winapi-0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-more-asserts-0.2
  (package
    (name "rust-more-asserts")
    (version "0.2.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "more-asserts" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "12b3fypg2sr4qmxy6wiyx6k9sdg573f5ij98cdmbrg00whnyqhvq"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-multimap-0.8
  (package
    (name "rust-multimap")
    (version "0.8.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "multimap" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0sicyz4n500vdhgcxn4g8jz97cp1ijir1rnbgph3pmx9ckz4dkp5"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-names-0.14
  (package
    (name "rust-names")
    (version "0.14.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "names" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1g1rxifcsvj9zj2nmwbdix8b5ynpghs4rq40vs966jqlylxwvpbv"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-rand" ,rust-rand-0.8))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-nix-0.23
  (package
    (name "rust-nix")
    (version "0.23.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "nix" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1iimixk7y2qk0jswqich4mkd8kqyzdghcgy6203j8fmxmhbn71lz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-memoffset" ,rust-memoffset-0.6)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-cc" ,rust-cc-1)
         ("rust-bitflags" ,rust-bitflags-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-nix-0.24
  (package
    (name "rust-nix")
    (version "0.24.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "nix" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1z35n1bhzslr7zawy2c0fl90jjy9l5b3lnsidls3908vfk0xnp0r"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-memoffset" ,rust-memoffset-0.6)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-bitflags" ,rust-bitflags-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-nom-5
  (package
    (name "rust-nom")
    (version "5.1.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "nom" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1br74rwdp3c2ddga03bphnf355spn4mzwf1slg0a30zd4qnjdd7z"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-version-check" ,rust-version-check-0.9)
         ("rust-memchr" ,rust-memchr-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-nom-7
  (package
    (name "rust-nom")
    (version "7.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "nom" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0djc3lq5xihnwhrvkc4bj0fd58sjf632yh6hfiw545x355d3x458"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-minimal-lexical"
          ,rust-minimal-lexical-0.2)
         ("rust-memchr" ,rust-memchr-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-notify-6
  (package
    (name "rust-notify")
    (version "6.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "notify" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "18ijj0qpkra1gr61w4wn6pvcjjmab1qcvxffr0kwl66y6k3sd6sd"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-windows-sys" ,rust-windows-sys-0.45)
         ("rust-walkdir" ,rust-walkdir-2)
         ("rust-mio" ,rust-mio-0.8)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-kqueue" ,rust-kqueue-1)
         ("rust-inotify" ,rust-inotify-0.9)
         ("rust-fsevent-sys" ,rust-fsevent-sys-4)
         ("rust-filetime" ,rust-filetime-0.2)
         ("rust-crossbeam-channel"
          ,rust-crossbeam-channel-0.5)
         ("rust-bitflags" ,rust-bitflags-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-notify-debouncer-full-0.1
  (package
    (name "rust-notify-debouncer-full")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "notify-debouncer-full" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "06a2wsi514dhrq8q5ghsvkgwj7n0pliid5plipxpdrwvnhg2r0gl"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-walkdir" ,rust-walkdir-2)
         ("rust-parking-lot" ,rust-parking-lot-0.12)
         ("rust-notify" ,rust-notify-6)
         ("rust-file-id" ,rust-file-id-0.1)
         ("rust-crossbeam-channel"
          ,rust-crossbeam-channel-0.5))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-ntapi-0.3
  (package
    (name "rust-ntapi")
    (version "0.3.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ntapi" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "03wqq2x5jv5xrsirbxmm460gcfmpakjpq8yqmc5lzfrgznkp91y2"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-winapi" ,rust-winapi-0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-num-derive-0.3
  (package
    (name "rust-num-derive")
    (version "0.3.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "num-derive" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0gbl94ckzqjdzy4j8b1p55mz01g6n1l9bckllqvaj0wfz7zm6sl7"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-syn" ,rust-syn-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-num-integer-0.1
  (package
    (name "rust-num-integer")
    (version "0.1.45")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "num-integer" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1ncwavvwdmsqzxnn65phv6c6nn72pnv9xhpmjd6a429mzf4k6p92"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-num-traits" ,rust-num-traits-0.2)
         ("rust-autocfg" ,rust-autocfg-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-num-traits-0.2
  (package
    (name "rust-num-traits")
    (version "0.2.15")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "num-traits" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1kfdqqw2ndz0wx2j75v9nbjx7d3mh3150zs4p5595y02rwsdx3jp"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-autocfg" ,rust-autocfg-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-num-cpus-1
  (package
    (name "rust-num-cpus")
    (version "1.13.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "num_cpus" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "18apx62z4j4lajj2fi6r1i8slr9rs2d0xrbj2ls85qfyxck4brhr"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2)
         ("rust-hermit-abi" ,rust-hermit-abi-0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-object-0.28
  (package
    (name "rust-object")
    (version "0.28.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "object" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0964501nlfh806mik3f9v6n05mx74qa0w7byvn0sqpwm5lprhb74"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-memchr" ,rust-memchr-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-once-cell-1
  (package
    (name "rust-once-cell")
    (version "1.18.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "once_cell" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0vapcd5ambwck95wyz3ymlim35jirgnqn9a0qmi19msymv95v2yx"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-opaque-debug-0.2
  (package
    (name "rust-opaque-debug")
    (version "0.2.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "opaque-debug" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "172j6bs8ndclqxa2m64qc0y1772rr73g4l9fg2svscgicnbfff98"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-opaque-debug-0.3
  (package
    (name "rust-opaque-debug")
    (version "0.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "opaque-debug" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1m8kzi4nd6shdqimn0mgb24f0hxslhnqd1whakyq06wcqd086jk2"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-openssl-probe-0.1
  (package
    (name "rust-openssl-probe")
    (version "0.1.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "openssl-probe" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1kq18qm48rvkwgcggfkqq6pm948190czqc94d6bm2sir5hq1l0gz"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-openssl-src-300
  (package
    (name "rust-openssl-src")
    (version "300.1.6+3.1.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "openssl-src" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "11c0p3vapkcd7090j11rpnsv6fk4wkfqa336ld179kcjw19sr7s3"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cc" ,rust-cc-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-openssl-sys-0.9
  (package
    (name "rust-openssl-sys")
    (version "0.9.93")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "openssl-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "078vnn4s18kj8m5sd7b684frhjnxjcjc9z7s7h4871s7q2j5ckfv"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-vcpkg" ,rust-vcpkg-0.2)
         ("rust-pkg-config" ,rust-pkg-config-0.3)
         ("rust-openssl-src" ,rust-openssl-src-300)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-cc" ,rust-cc-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-option-ext-0.2
  (package
    (name "rust-option-ext")
    (version "0.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "option-ext" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0zbf7cx8ib99frnlanpyikm1bx8qn8x602sw1n7bg6p9x94lyx04"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-ordered-float-2
  (package
    (name "rust-ordered-float")
    (version "2.10.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ordered-float" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "11qdskfgk911bs541avzkrfahq6arnb2bkvzs0c36na2m4ncyh3r"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-num-traits" ,rust-num-traits-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-ordered-float-3
  (package
    (name "rust-ordered-float")
    (version "3.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ordered-float" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "19rsn0vjiiki3qnavhbs2n27f26zyqzglmxjw92yr41z34qf6x0z"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-num-traits" ,rust-num-traits-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-os-str-bytes-6
  (package
    (name "rust-os-str-bytes")
    (version "6.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "os_str_bytes" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1ykav0kv9152z4cmv8smwmd9pac9q42sihi4wphnrzlwx4c6hci1"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-owo-colors-3
  (package
    (name "rust-owo-colors")
    (version "3.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "owo-colors" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0yx8glp9vipa6gybbqmpg8lyrcsrv9z6dia94p5lvshzja0p7kyy"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-parking-2
  (package
    (name "rust-parking")
    (version "2.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "parking" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0wnxxnizfxlax3n709s5r83f4n8awy3m4a18q4fdk0z7z693hz22"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-parking-lot-0.11
  (package
    (name "rust-parking-lot")
    (version "0.11.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "parking_lot" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "16gzf41bxmm10x82bla8d6wfppy9ym3fxsmdjyvn61m66s0bf5vx"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-parking-lot-core"
          ,rust-parking-lot-core-0.8)
         ("rust-lock-api" ,rust-lock-api-0.4)
         ("rust-instant" ,rust-instant-0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-parking-lot-0.12
  (package
    (name "rust-parking-lot")
    (version "0.12.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "parking_lot" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "13r2xk7mnxfc5g0g6dkdxqdqad99j7s7z8zhzz4npw5r0g0v4hip"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-parking-lot-core"
          ,rust-parking-lot-core-0.9)
         ("rust-lock-api" ,rust-lock-api-0.4))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-parking-lot-core-0.8
  (package
    (name "rust-parking-lot-core")
    (version "0.8.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "parking_lot_core" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "05ij4zxsylx99srbq8qd1k2wiwaq8krkf9y4cqkhvb5wjca8wvnp"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-winapi" ,rust-winapi-0.3)
         ("rust-smallvec" ,rust-smallvec-1)
         ("rust-redox-syscall" ,rust-redox-syscall-0.2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-instant" ,rust-instant-0.1)
         ("rust-cfg-if" ,rust-cfg-if-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-parking-lot-core-0.9
  (package
    (name "rust-parking-lot-core")
    (version "0.9.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "parking_lot_core" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "13h0imw1aq86wj28gxkblhkzx6z1gk8q18n0v76qmmj6cliajhjc"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-windows-targets"
          ,rust-windows-targets-0.48)
         ("rust-smallvec" ,rust-smallvec-1)
         ("rust-redox-syscall" ,rust-redox-syscall-0.4)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-cfg-if" ,rust-cfg-if-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-paste-1
  (package
    (name "rust-paste")
    (version "1.0.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "paste" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1z15h1rnq1wcacpcvgm77djl3413gs1nlhmn90qpcvjx2c2hwlhc"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-percent-encoding-2
  (package
    (name "rust-percent-encoding")
    (version "2.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "percent-encoding" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0bp3zrsk3kr47fbpipyczidbbx4g54lzxdm77ni1i3qws10mdzfl"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-pest-2
  (package
    (name "rust-pest")
    (version "2.1.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "pest" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0lry80bm90x47nq71wxq83kjrm9ashpz4kbm92p90ysdx4m8gx0h"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-ucd-trie" ,rust-ucd-trie-0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-pest-derive-2
  (package
    (name "rust-pest-derive")
    (version "2.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "pest_derive" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1l5jfa6ril71cw5nsiw0r45br54dd8cj2r1nc2d1wq6wb3jilgc3"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-pest-generator" ,rust-pest-generator-2)
         ("rust-pest" ,rust-pest-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-pest-generator-2
  (package
    (name "rust-pest-generator")
    (version "2.1.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "pest_generator" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0mfgl0p6v91ywdqr9i8w053v70cnfqjk8y5rhwbvir9idridpf4r"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-syn" ,rust-syn-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-pest-meta" ,rust-pest-meta-2)
         ("rust-pest" ,rust-pest-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-pest-meta-2
  (package
    (name "rust-pest-meta")
    (version "2.1.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "pest_meta" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "07d1jbbbpxpchk0j37ljas46sdyyg599z3zw2ac0f5sk9x06xgjl"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-sha-1" ,rust-sha-1-0.8)
         ("rust-pest" ,rust-pest-2)
         ("rust-maplit" ,rust-maplit-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-petgraph-0.6
  (package
    (name "rust-petgraph")
    (version "0.6.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "petgraph" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1912xw827flj8mzqm62jcbg0cv54qfhzm48c13ilzr9px67d5msd"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-indexmap" ,rust-indexmap-1)
         ("rust-fixedbitset" ,rust-fixedbitset-0.4))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-phf-0.8
  (package
    (name "rust-phf")
    (version "0.8.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "phf" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "04pyv8bzqvw69rd5dynd5nb85py1hf7wa4ixyhrvdz1l5qin3yrx"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-phf-shared" ,rust-phf-shared-0.8))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-phf-0.10
  (package
    (name "rust-phf")
    (version "0.10.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "phf" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0naj8n5nasv5hj5ldlva3cl6y3sv7zp3kfgqylhbrg55v3mg3fzs"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-phf-shared" ,rust-phf-shared-0.10))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-phf-0.11
  (package
    (name "rust-phf")
    (version "0.11.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "phf" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1354fbpb52cp9gs5mlkaygc5qhdx6r07rfv3xy482m4kvqsnb34j"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-phf-shared" ,rust-phf-shared-0.11)
         ("rust-phf-macros" ,rust-phf-macros-0.11))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-phf-codegen-0.8
  (package
    (name "rust-phf-codegen")
    (version "0.8.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "phf_codegen" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "05d8w7aqqjb6039pfm6404gk5dlwrrf97kiy1n21212vb1hyxzyb"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-phf-shared" ,rust-phf-shared-0.8)
         ("rust-phf-generator" ,rust-phf-generator-0.8))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-phf-generator-0.8
  (package
    (name "rust-phf-generator")
    (version "0.8.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "phf_generator" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "09i5338d1kixq6a60fcayz6awgxjlxcfw9ic5f02abbgr067ydhp"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-rand" ,rust-rand-0.7)
         ("rust-phf-shared" ,rust-phf-shared-0.8))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-phf-generator-0.11
  (package
    (name "rust-phf-generator")
    (version "0.11.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "phf_generator" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1gsgy5k45y937qnwp58dc05d63lwlfm3imqr1zslb8qgb2a1q65i"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-rand" ,rust-rand-0.8)
         ("rust-phf-shared" ,rust-phf-shared-0.11))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-phf-macros-0.11
  (package
    (name "rust-phf-macros")
    (version "0.11.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "phf_macros" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0rncvjimjri2vancig85icbk8h03a5s3z4cyasd70s37y72wvalj"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-syn" ,rust-syn-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-phf-shared" ,rust-phf-shared-0.11)
         ("rust-phf-generator" ,rust-phf-generator-0.11))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-phf-shared-0.8
  (package
    (name "rust-phf-shared")
    (version "0.8.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "phf_shared" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1xssnqrrcn0nr9ayqrnm8xm37ac4xvwcx8pax7jxss7yxawzh360"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-siphasher" ,rust-siphasher-0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-phf-shared-0.10
  (package
    (name "rust-phf-shared")
    (version "0.10.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "phf_shared" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "15n02nc8yqpd8hbxngblar2g53p3nllc93d8s8ih3p5cf7bnlydn"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-siphasher" ,rust-siphasher-0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-phf-shared-0.11
  (package
    (name "rust-phf-shared")
    (version "0.11.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "phf_shared" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0xp6krf3cd411rz9rbk7p6xprlz786a215039j6jlxvbh9pmzyz1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-siphasher" ,rust-siphasher-0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-pin-project-1
  (package
    (name "rust-pin-project")
    (version "1.1.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "pin-project" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "08k4cpy8q3j93qqgnrbzkcgpn7g0a88l4a9nm33kyghpdhffv97x"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-pin-project-internal"
          ,rust-pin-project-internal-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-pin-project-internal-1
  (package
    (name "rust-pin-project-internal")
    (version "1.1.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "pin-project-internal" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "01a4l3vb84brv9v7wl71chzxra2kynm6yvcjca66xv3ij6fgsna3"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-syn" ,rust-syn-2)
         ("rust-quote" ,rust-quote-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-pin-project-lite-0.2
  (package
    (name "rust-pin-project-lite")
    (version "0.2.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "pin-project-lite" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "05n1z851l356hpgqadw4ar64mjanaxq1qlwqsf2k05ziq8xax9z0"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-pin-utils-0.1
  (package
    (name "rust-pin-utils")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "pin-utils" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "117ir7vslsl2z1a7qzhws4pd01cg2d3338c47swjyvqv2n60v1wb"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-pkg-config-0.3
  (package
    (name "rust-pkg-config")
    (version "0.3.25")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "pkg-config" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1bh3vij79cshj884py4can1f8rvk52niaii1vwxya9q69gnc9y0x"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-polling-2
  (package
    (name "rust-polling")
    (version "2.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "polling" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0najvslgg2zclsnxay071jdw0bna7f0abqvgx3isv7w817ah8m38"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-winapi" ,rust-winapi-0.3)
         ("rust-wepoll-ffi" ,rust-wepoll-ffi-0.1)
         ("rust-log" ,rust-log-0.4)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-cfg-if" ,rust-cfg-if-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-polyval-0.4
  (package
    (name "rust-polyval")
    (version "0.4.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "polyval" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1kdpcjhc3666g8xaqichsyf6fhn8rry3z70dqhmvv6hb2jmc9g7f"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-universal-hash" ,rust-universal-hash-0.4)
         ("rust-opaque-debug" ,rust-opaque-debug-0.3)
         ("rust-cpuid-bool" ,rust-cpuid-bool-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-ppv-lite86-0.2
  (package
    (name "rust-ppv-lite86")
    (version "0.2.16")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ppv-lite86" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0wkqwnvnfcgqlrahphl45vdlgi2f1bs7nqcsalsllp1y4dp9x7zb"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-pretty-bytes-0.2
  (package
    (name "rust-pretty-bytes")
    (version "0.2.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "pretty-bytes" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "05jkd1f824b93jh0jwfskba9hd70crvjz2nl1hf2xgqx5kfnx780"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-getopts" ,rust-getopts-0.2)
         ("rust-atty" ,rust-atty-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-prettyplease-0.1
  (package
    (name "rust-prettyplease")
    (version "0.1.25")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "prettyplease" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "11lskniv8pf8y8bn4dc3nmjapfhnibxbm5gamp2ad9qna3lld1kc"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-syn" ,rust-syn-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-proc-macro-error-1
  (package
    (name "rust-proc-macro-error")
    (version "1.0.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "proc-macro-error" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1373bhxaf0pagd8zkyd03kkx6bchzf6g0dkwrwzsnal9z47lj9fs"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-version-check" ,rust-version-check-0.9)
         ("rust-syn" ,rust-syn-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-proc-macro-error-attr"
          ,rust-proc-macro-error-attr-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-proc-macro-error-attr-1
  (package
    (name "rust-proc-macro-error-attr")
    (version "1.0.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "proc-macro-error-attr" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0sgq6m5jfmasmwwy8x4mjygx5l7kp8s4j60bv25ckv2j1qc41gm1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-version-check" ,rust-version-check-0.9)
         ("rust-quote" ,rust-quote-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-proc-macro-hack-0.5
  (package
    (name "rust-proc-macro-hack")
    (version "0.5.19")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "proc-macro-hack" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1rg0kzsj7lj00qj602d3h77spwfz48vixn1wbjp7a4yrq65w9w6v"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-proc-macro2-1
  (package
    (name "rust-proc-macro2")
    (version "1.0.69")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "proc-macro2" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1nljgyllbm3yr3pa081bf83gxh6l4zvjqzaldw7v4mj9xfgihk0k"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-unicode-ident" ,rust-unicode-ident-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-prost-0.11
  (package
    (name "rust-prost")
    (version "0.11.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "prost" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1kc1hva2h894hc0zf6r4r8fsxfpazf7xn5rj3jya9sbrsyhym0hb"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-prost-derive" ,rust-prost-derive-0.11)
         ("rust-bytes" ,rust-bytes-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-prost-build-0.11
  (package
    (name "rust-prost-build")
    (version "0.11.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "prost-build" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0w5jx97q96ydhkg67wx3lb11kfy8195c56g0476glzws5iak758i"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-which" ,rust-which-4)
         ("rust-tempfile" ,rust-tempfile-3)
         ("rust-syn" ,rust-syn-1)
         ("rust-regex" ,rust-regex-1)
         ("rust-prost-types" ,rust-prost-types-0.11)
         ("rust-prost" ,rust-prost-0.11)
         ("rust-prettyplease" ,rust-prettyplease-0.1)
         ("rust-petgraph" ,rust-petgraph-0.6)
         ("rust-multimap" ,rust-multimap-0.8)
         ("rust-log" ,rust-log-0.4)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-itertools" ,rust-itertools-0.10)
         ("rust-heck" ,rust-heck-0.4)
         ("rust-bytes" ,rust-bytes-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-prost-derive-0.11
  (package
    (name "rust-prost-derive")
    (version "0.11.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "prost-derive" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1d3mw2s2jba1f7wcjmjd6ha2a255p2rmynxhm1nysv9w1z8xilp5"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-syn" ,rust-syn-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-itertools" ,rust-itertools-0.10)
         ("rust-anyhow" ,rust-anyhow-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-prost-types-0.11
  (package
    (name "rust-prost-types")
    (version "0.11.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "prost-types" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "04ryk38sqkp2nf4dgdqdfbgn6zwwvjraw6hqq6d9a6088shj4di1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-prost" ,rust-prost-0.11))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-ptr-meta-0.1
  (package
    (name "rust-ptr-meta")
    (version "0.1.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ptr_meta" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1wd4wy0wxrcays4f1gy8gwcmxg7mskmivcv40p0hidh6xbvwqf07"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-ptr-meta-derive"
          ,rust-ptr-meta-derive-0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-ptr-meta-derive-0.1
  (package
    (name "rust-ptr-meta-derive")
    (version "0.1.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ptr_meta_derive" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1b69cav9wn67cixshizii0q5mlbl0lihx706vcrzm259zkdlbf0n"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-syn" ,rust-syn-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-pulldown-cmark-0.8
  (package
    (name "rust-pulldown-cmark")
    (version "0.8.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "pulldown-cmark" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1y6wh446g6vravvj70zsadzswyl2b4pyln9ib76m697jjljf1bgz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-unicase" ,rust-unicase-2)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-bitflags" ,rust-bitflags-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-quote-1
  (package
    (name "rust-quote")
    (version "1.0.27")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "quote" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "004mdlsn61k3f9lqv4yk8ghbzq6x1r2m9in7hg2c2pi68p8jjkwg"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-rand-0.7
  (package
    (name "rust-rand")
    (version "0.7.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rand" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "00sdaimkbz491qgi6qxkv582yivl32m2jd401kzbn94vsiwicsva"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-rand-pcg" ,rust-rand-pcg-0.2)
         ("rust-rand-hc" ,rust-rand-hc-0.2)
         ("rust-rand-core" ,rust-rand-core-0.5)
         ("rust-rand-chacha" ,rust-rand-chacha-0.2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-getrandom" ,rust-getrandom-0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-rand-0.8
  (package
    (name "rust-rand")
    (version "0.8.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rand" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "013l6931nn7gkc23jz5mm3qdhf93jjf0fg64nz2lp4i51qd8vbrl"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-rand-core" ,rust-rand-core-0.6)
         ("rust-rand-chacha" ,rust-rand-chacha-0.3)
         ("rust-libc" ,rust-libc-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-rand-chacha-0.2
  (package
    (name "rust-rand-chacha")
    (version "0.2.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rand_chacha" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "00il36fkdbsmpr99p9ksmmp6dn1md7rmnwmz0rr77jbrca2yvj7l"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-rand-core" ,rust-rand-core-0.5)
         ("rust-ppv-lite86" ,rust-ppv-lite86-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-rand-chacha-0.3
  (package
    (name "rust-rand-chacha")
    (version "0.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rand_chacha" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "123x2adin558xbhvqb8w4f6syjsdkmqff8cxwhmjacpsl1ihmhg6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-rand-core" ,rust-rand-core-0.6)
         ("rust-ppv-lite86" ,rust-ppv-lite86-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-rand-core-0.5
  (package
    (name "rust-rand-core")
    (version "0.5.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rand_core" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "06bdvx08v3rkz451cm7z59xwwqn1rkfh6v9ay77b14f8dwlybgch"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-getrandom" ,rust-getrandom-0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-rand-core-0.6
  (package
    (name "rust-rand-core")
    (version "0.6.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rand_core" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1rxlxc3bpzgwphcg9c9yasvv9idipcg2z2y4j0vlb52jyl418kyk"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-getrandom" ,rust-getrandom-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-rand-hc-0.2
  (package
    (name "rust-rand-hc")
    (version "0.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rand_hc" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0g31sqwpmsirdlwr0svnacr4dbqyz339im4ssl9738cjgfpjjcfa"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-rand-core" ,rust-rand-core-0.5))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-rand-pcg-0.2
  (package
    (name "rust-rand-pcg")
    (version "0.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rand_pcg" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0ab4h6s6x3py833jk61lwadq83qd1c8bih2hgi6yps9rnv0x1aqn"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-rand-core" ,rust-rand-core-0.5))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-rayon-1
  (package
    (name "rust-rayon")
    (version "1.5.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rayon" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0z9sjcy1hnnvgkwx3cn1x44pf24jpwarp3172m9am2xd5rvyb6dx"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-rayon-core" ,rust-rayon-core-1)
         ("rust-either" ,rust-either-1)
         ("rust-crossbeam-deque"
          ,rust-crossbeam-deque-0.8)
         ("rust-autocfg" ,rust-autocfg-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-rayon-core-1
  (package
    (name "rust-rayon-core")
    (version "1.9.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rayon-core" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0gv8k6612gc24kqqm4440f5qfx6gnyv2v6dj3d4libbdmjswv2r5"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-num-cpus" ,rust-num-cpus-1)
         ("rust-crossbeam-utils"
          ,rust-crossbeam-utils-0.8)
         ("rust-crossbeam-deque"
          ,rust-crossbeam-deque-0.8)
         ("rust-crossbeam-channel"
          ,rust-crossbeam-channel-0.5))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-redox-syscall-0.2
  (package
    (name "rust-redox-syscall")
    (version "0.2.13")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "redox_syscall" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0hpgwvgjlg1j9z7bjf5y18fkd8ag7y4znhqxg85hnpp5qz25pwk2"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-redox-syscall-0.4
  (package
    (name "rust-redox-syscall")
    (version "0.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "redox_syscall" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1aiifyz5dnybfvkk4cdab9p2kmphag1yad6iknc7aszlxxldf8j7"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-redox-users-0.4
  (package
    (name "rust-redox-users")
    (version "0.4.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "redox_users" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0asw3s4iy69knafkhvlbchy230qawc297vddjdwjs5nglwvxhcxh"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-thiserror" ,rust-thiserror-1)
         ("rust-redox-syscall" ,rust-redox-syscall-0.2)
         ("rust-getrandom" ,rust-getrandom-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-regalloc2-0.3
  (package
    (name "rust-regalloc2")
    (version "0.3.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "regalloc2" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0yd76dw8b0wr35m316y11xg4yygis0qn6i1xkw3yyy6ray920fnl"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-smallvec" ,rust-smallvec-1)
         ("rust-slice-group-by" ,rust-slice-group-by-0.3)
         ("rust-log" ,rust-log-0.4)
         ("rust-fxhash" ,rust-fxhash-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-regex-1
  (package
    (name "rust-regex")
    (version "1.8.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "regex" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0w2kgdvs2fsv39hrsb912zjq3bx5vw1cchslvbi6mk1iycbyd0xg"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-regex-syntax" ,rust-regex-syntax-0.7)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-aho-corasick" ,rust-aho-corasick-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-regex-syntax-0.7
  (package
    (name "rust-regex-syntax")
    (version "0.7.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "regex-syntax" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0g1s6ra0ra8xy1fxscspd406c3pn53bjm1is8phamlwvy6a656d5"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-region-3
  (package
    (name "rust-region")
    (version "3.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "region" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0bji1p0c9abzh78ps5hs0ygg9pxkg7gjspll43lxr14q6v18kqbn"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-winapi" ,rust-winapi-0.3)
         ("rust-mach" ,rust-mach-0.3)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-bitflags" ,rust-bitflags-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-remove-dir-all-0.5
  (package
    (name "rust-remove-dir-all")
    (version "0.5.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "remove_dir_all" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1rzqbsgkmr053bxxl04vmvsd1njyz0nxvly97aip6aa2cmb15k9s"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-winapi" ,rust-winapi-0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-rend-0.3
  (package
    (name "rust-rend")
    (version "0.3.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rend" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "15fz3rw8c74586kxl6dcdn4s864ph884wfpg9shgnbrnnss69bvr"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bytecheck" ,rust-bytecheck-0.6))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-rkyv-0.7
  (package
    (name "rust-rkyv")
    (version "0.7.39")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rkyv" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "05gdspzw03hq6l58si4ixfj5xd27ljw6fiqksggnvn87bd4b7hnf"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-seahash" ,rust-seahash-4)
         ("rust-rkyv-derive" ,rust-rkyv-derive-0.7)
         ("rust-rend" ,rust-rend-0.3)
         ("rust-ptr-meta" ,rust-ptr-meta-0.1)
         ("rust-indexmap" ,rust-indexmap-1)
         ("rust-hashbrown" ,rust-hashbrown-0.12)
         ("rust-bytecheck" ,rust-bytecheck-0.6))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-rkyv-derive-0.7
  (package
    (name "rust-rkyv-derive")
    (version "0.7.39")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rkyv_derive" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1i1lmir3lm8zj8k1an7j2rchv1admqhysh6r6bfkcgmmi3fdmbkf"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-syn" ,rust-syn-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-rmp-0.8
  (package
    (name "rust-rmp")
    (version "0.8.11")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rmp" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "17rw803xv84csxgd654g7q64kqf9zgkvhsn8as3dbmlg6mr92la4"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-paste" ,rust-paste-1)
         ("rust-num-traits" ,rust-num-traits-0.2)
         ("rust-byteorder" ,rust-byteorder-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-rmp-serde-1
  (package
    (name "rust-rmp-serde")
    (version "1.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rmp-serde" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0zilg9zhx2pmyd889hnnzcfzf34hk49g7wynldgij4314w6nny15"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde" ,rust-serde-1)
         ("rust-rmp" ,rust-rmp-0.8)
         ("rust-byteorder" ,rust-byteorder-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-rustc-demangle-0.1
  (package
    (name "rust-rustc-demangle")
    (version "0.1.21")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rustc-demangle" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0hn3xyd2n3bg3jnc5a5jbzll32n4r5a65bqzs287l30m5c53xw3y"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-rustc-version-0.2
  (package
    (name "rust-rustc-version")
    (version "0.2.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rustc_version" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "02h3x57lcr8l2pm0a645s9whdh33pn5cnrwvn5cb57vcrc53x3hk"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-semver" ,rust-semver-0.9))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-rustc-version-0.4
  (package
    (name "rust-rustc-version")
    (version "0.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rustc_version" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0rpk9rcdk405xhbmgclsh4pai0svn49x35aggl4nhbkd4a2zb85z"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-semver" ,rust-semver-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-rustix-0.37
  (package
    (name "rust-rustix")
    (version "0.37.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rustix" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "07g1395z8a66i13bihv8qkzd6fc1rhriih71fj93xmmk96787bia"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-windows-sys" ,rust-windows-sys-0.45)
         ("rust-linux-raw-sys" ,rust-linux-raw-sys-0.3)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-io-lifetimes" ,rust-io-lifetimes-1)
         ("rust-errno" ,rust-errno-0.3)
         ("rust-bitflags" ,rust-bitflags-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-ryu-1
  (package
    (name "rust-ryu")
    (version "1.0.10")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ryu" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "15960rzj6jkjhxrjfr3kid2hbnia84s6h8l1ga7vkla9rwmgkxpk"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-same-file-1
  (package
    (name "rust-same-file")
    (version "1.0.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "same-file" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "00h5j1w87dmhnvbv9l8bic3y7xxsnjmssvifw2ayvgx9mb1ivz4k"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-winapi-util" ,rust-winapi-util-0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-schannel-0.1
  (package
    (name "rust-schannel")
    (version "0.1.22")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "schannel" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "126zy5jb95fc5hvzyjwiq6lc81r08rdcn6affn00ispp9jzk6dqc"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-windows-sys" ,rust-windows-sys-0.48))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-scopeguard-1
  (package
    (name "rust-scopeguard")
    (version "1.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "scopeguard" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1kbqm85v43rq92vx7hfiay6pmcga03vrjbbfwqpyj3pwsg3b16nj"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-seahash-4
  (package
    (name "rust-seahash")
    (version "4.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "seahash" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0sxsb64np6bvnppjz5hg4rqpnkczhsl8w8kf2a5lr1c08xppn40w"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-semver-0.9
  (package
    (name "rust-semver")
    (version "0.9.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "semver" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "00q4lkcj0rrgbhviv9sd4p6qmdsipkwkbra7rh11jrhq5kpvjzhx"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-semver-parser" ,rust-semver-parser-0.7))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-semver-0.11
  (package
    (name "rust-semver")
    (version "0.11.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "semver" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1dn6064fipjymnmjccyjhb70miyvqvp08gvw1wbg8vbg4c8ay0gk"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-semver-parser" ,rust-semver-parser-0.10))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-semver-1
  (package
    (name "rust-semver")
    (version "1.0.17")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "semver" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1vf03d99sikkradjj33q1l9h4fqgd1h7darjypic6pnh4qrkdgdy"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-semver-parser-0.7
  (package
    (name "rust-semver-parser")
    (version "0.7.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "semver-parser" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "18vhypw6zgccnrlm5ps1pwa0khz7ry927iznpr88b87cagr1v2iq"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-semver-parser-0.10
  (package
    (name "rust-semver-parser")
    (version "0.10.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "semver-parser" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1xqijhqhx3bn77xnl1mlcp032hz8nv7n2fbdacbdzq7rnzsvxc00"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-pest" ,rust-pest-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-serde-1
  (package
    (name "rust-serde")
    (version "1.0.137")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "serde" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1l8pynxnmld179a33l044yvkigq3fhiwgx0518a1b0vzqxa8vsk1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde-derive" ,rust-serde-derive-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-serde-value-0.7
  (package
    (name "rust-serde-value")
    (version "0.7.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "serde-value" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0b18ngk7n4f9zmwsfdkhgsp31192smzyl5z143qmx1qi28sa78gk"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde" ,rust-serde-1)
         ("rust-ordered-float" ,rust-ordered-float-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-serde-wasm-bindgen-0.4
  (package
    (name "rust-serde-wasm-bindgen")
    (version "0.4.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "serde-wasm-bindgen" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1gr8hrr2zx9wqq02vh5lmsyhyaf0agvapf42glq1940drlqw1d73"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2)
         ("rust-serde" ,rust-serde-1)
         ("rust-js-sys" ,rust-js-sys-0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-serde-derive-1
  (package
    (name "rust-serde-derive")
    (version "1.0.137")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "serde_derive" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1gkqhpw86zvppd0lwa8ljzpglwczxq3d7cnkfwirfn9r1jxgl9hz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-syn" ,rust-syn-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-serde-json-1
  (package
    (name "rust-serde-json")
    (version "1.0.81")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "serde_json" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0p7100hlvw4azgcalzf1vgray5cg6b6saqfwb32h7v8s5ary4z4v"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde" ,rust-serde-1)
         ("rust-ryu" ,rust-ryu-1)
         ("rust-itoa" ,rust-itoa-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-serde-qs-0.8
  (package
    (name "rust-serde-qs")
    (version "0.8.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "serde_qs" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "19pngnlga4xnap85kdvn661662hf42lkkppp9sd04py7xs056wf7"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-thiserror" ,rust-thiserror-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-percent-encoding"
          ,rust-percent-encoding-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-serde-urlencoded-0.7
  (package
    (name "rust-serde-urlencoded")
    (version "0.7.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "serde_urlencoded" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1zgklbdaysj3230xivihs30qi5vkhigg323a9m62k8jwf4a1qjfk"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde" ,rust-serde-1)
         ("rust-ryu" ,rust-ryu-1)
         ("rust-itoa" ,rust-itoa-1)
         ("rust-form-urlencoded" ,rust-form-urlencoded-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-serde-yaml-0.8
  (package
    (name "rust-serde-yaml")
    (version "0.8.24")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "serde_yaml" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1g5f4hlk2wlvwm3hdisd5r99iic8if1pqwrb6cl6dnqmaj4iazbh"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-yaml-rust" ,rust-yaml-rust-0.4)
         ("rust-serde" ,rust-serde-1)
         ("rust-ryu" ,rust-ryu-1)
         ("rust-indexmap" ,rust-indexmap-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-session-manager-0.1
  (package
    (name "rust-session-manager")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "session-manager" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0000000000000000000000000000000000000000000000000000"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-zellij-tile" ,rust-zellij-tile-0.40)
         ("rust-uuid" ,rust-uuid-1)
         ("rust-unicode-width" ,rust-unicode-width-0.1)
         ("rust-humantime" ,rust-humantime-2)
         ("rust-fuzzy-matcher" ,rust-fuzzy-matcher-0.3)
         ("rust-chrono" ,rust-chrono-0.4)
         ("rust-ansi-term" ,rust-ansi-term-0.12))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-sha-1-0.8
  (package
    (name "rust-sha-1")
    (version "0.8.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "sha-1" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1pv387q0r7llk2cqzyq0nivzvkgqgzsiygqzlv7b68z9xl5lvngp"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-opaque-debug" ,rust-opaque-debug-0.2)
         ("rust-fake-simd" ,rust-fake-simd-0.1)
         ("rust-digest" ,rust-digest-0.8)
         ("rust-block-buffer" ,rust-block-buffer-0.7))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-sha1-0.6
  (package
    (name "rust-sha1")
    (version "0.6.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "sha1" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0w1p0s9060cv1vlgfa5c93kjksmvzjjc8j780lns3jj5fk4hbnn1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-sha1-smol" ,rust-sha1-smol-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-sha1-smol-1
  (package
    (name "rust-sha1-smol")
    (version "1.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "sha1_smol" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "04nhbhvsk5ms1zbshs80iq5r1vjszp2xnm9f0ivj38q3dhc4f6mf"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-sha2-0.9
  (package
    (name "rust-sha2")
    (version "0.9.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "sha2" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "006q2f0ar26xcjxqz8zsncfgz86zqa5dkwlwv03rhx1rpzhs2n2d"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-opaque-debug" ,rust-opaque-debug-0.3)
         ("rust-digest" ,rust-digest-0.9)
         ("rust-cpufeatures" ,rust-cpufeatures-0.2)
         ("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-block-buffer" ,rust-block-buffer-0.9))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-shell-words-1
  (package
    (name "rust-shell-words")
    (version "1.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "shell-words" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1plgwx8r0h5ismbbp6cp03740wmzgzhip85k5hxqrrkaddkql614"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-shellexpand-3
  (package
    (name "rust-shellexpand")
    (version "3.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "shellexpand" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1qc0d1zsaha7hphmyp0323zbndby5c6hrz9r9i44sab5lvg7s76x"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-dirs" ,rust-dirs-4))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-signal-hook-0.1
  (package
    (name "rust-signal-hook")
    (version "0.1.17")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "signal-hook" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0a97migr3gcy4sfkgxdp29082s3f3lbf4was3dkpl13gq51d8cby"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-signal-hook-registry"
          ,rust-signal-hook-registry-1)
         ("rust-libc" ,rust-libc-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-signal-hook-0.3
  (package
    (name "rust-signal-hook")
    (version "0.3.14")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "signal-hook" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "17g2bc1c74m1zvnfxzwym0c8wczbvjg5qm3bq97ld616kvlbalx2"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-signal-hook-registry"
          ,rust-signal-hook-registry-1)
         ("rust-libc" ,rust-libc-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-signal-hook-registry-1
  (package
    (name "rust-signal-hook-registry")
    (version "1.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "signal-hook-registry" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1c2mhijg54y6c1zi4630yki1vpq3z96ljfnsrdy0rb64ilr767p5"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-similar-2
  (package
    (name "rust-similar")
    (version "2.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "similar" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1lw33na01r35h09s47jqhjgz3m29wapl20f6ybsla5d1cfgrf91f"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-siphasher-0.3
  (package
    (name "rust-siphasher")
    (version "0.3.10")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "siphasher" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1pi5sb2j2wi92zfqj6qxnk11vk1qq2plya5g2a5kzbwrd0hf7lvv"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-sixel-image-0.1
  (package
    (name "rust-sixel-image")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "sixel-image" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "17y9yykj03i47adh0bqqra659m8rd68yxmmsp50pgf26l1fhp244"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-sixel-tokenizer"
          ,rust-sixel-tokenizer-0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-sixel-tokenizer-0.1
  (package
    (name "rust-sixel-tokenizer")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "sixel-tokenizer" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0xnlg3vfmh96bqj1fnj6qdgjdnl0zc6v07ww2xh4v5mc55k7y6xp"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-thiserror" ,rust-thiserror-1)
         ("rust-arrayvec" ,rust-arrayvec-0.7))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-slab-0.4
  (package
    (name "rust-slab")
    (version "0.4.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "slab" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0cmvcy9ppsh3dz8mi6jljx7bxyknvgpas4aid2ayxk1vjpz3qw7b"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-slice-group-by-0.3
  (package
    (name "rust-slice-group-by")
    (version "0.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "slice-group-by" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "19vbyyxqvc25fv2dmhlxijlk5sa9j34yb6hyydb9vf89kh36fqc2"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-sluice-0.5
  (package
    (name "rust-sluice")
    (version "0.5.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "sluice" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1d9ywr5039ibgaby8sc72f8fs5lpp8j5y6p3npya4jplxz000x3d"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-futures-io" ,rust-futures-io-0.3)
         ("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-async-channel" ,rust-async-channel-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-smallvec-1
  (package
    (name "rust-smallvec")
    (version "1.8.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "smallvec" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "10zf4fn63p2d6sx8qap3jvyarcfw563308x3431hd4c34r35gpgj"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-smawk-0.3
  (package
    (name "rust-smawk")
    (version "0.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "smawk" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0hv0q1mw1r1brk7v3g4a80j162p7g1dri4bdidykrakzfqjd4ypn"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-socket2-0.4
  (package
    (name "rust-socket2")
    (version "0.4.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "socket2" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0qnn1r41jqj20m0a2nzzjgzndlmpg5maiyjchccaypfqxq8sk934"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-winapi" ,rust-winapi-0.3)
         ("rust-libc" ,rust-libc-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-spinning-0.1
  (package
    (name "rust-spinning")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "spinning" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0yrs2lzyyrwvs58pya2h22pfdx3vv0h76w1av5c2dbbw5630wkrd"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-lock-api" ,rust-lock-api-0.4))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-spinning-top-0.2
  (package
    (name "rust-spinning-top")
    (version "0.2.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "spinning_top" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1c6x734rlvvhjw1prk8k3y7d5z65459br6pzl2ila564yjib37jv"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-lock-api" ,rust-lock-api-0.4))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-ssh2-0.9
  (package
    (name "rust-ssh2")
    (version "0.9.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ssh2" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1cgfwarj5k8s9fbf1pcnp27ifk30xk2f7q3sjca7l1ih8kk474r6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-parking-lot" ,rust-parking-lot-0.11)
         ("rust-libssh2-sys" ,rust-libssh2-sys-0.2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-bitflags" ,rust-bitflags-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-stable-deref-trait-1
  (package
    (name "rust-stable-deref-trait")
    (version "1.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "stable_deref_trait" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1lxjr8q2n534b2lhkxd6l6wcddzjvnksi58zv11f9y0jjmr15wd8"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-standback-0.2
  (package
    (name "rust-standback")
    (version "0.2.17")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "standback" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1zr8zy3kzryaggz3k0j4135m3zbd31pyqmja8cyj8yp07mpzn4z1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-version-check" ,rust-version-check-0.9))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-status-bar-0.1
  (package
    (name "rust-status-bar")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "status-bar" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0000000000000000000000000000000000000000000000000000"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-zellij-tile-utils"
          ,rust-zellij-tile-utils-0.40)
         ("rust-zellij-tile" ,rust-zellij-tile-0.40)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-regex" ,rust-regex-1)
         ("rust-rand" ,rust-rand-0.8)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-colored" ,rust-colored-2)
         ("rust-ansi-term" ,rust-ansi-term-0.12))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-stdweb-0.4
  (package
    (name "rust-stdweb")
    (version "0.4.20")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "stdweb" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1md14n9rzxzdskz3hpgln8vxfwqsw2cswc0f5nslh4r82rmlj8nh"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2)
         ("rust-stdweb-internal-runtime"
          ,rust-stdweb-internal-runtime-0.1)
         ("rust-stdweb-internal-macros"
          ,rust-stdweb-internal-macros-0.2)
         ("rust-stdweb-derive" ,rust-stdweb-derive-0.5)
         ("rust-rustc-version" ,rust-rustc-version-0.2)
         ("rust-discard" ,rust-discard-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-stdweb-derive-0.5
  (package
    (name "rust-stdweb-derive")
    (version "0.5.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "stdweb-derive" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1vsh7g0gaxn4kxqq3knhymdn02p2pfxmnd2j0vplpj6c1yj60yn8"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-syn" ,rust-syn-1)
         ("rust-serde-derive" ,rust-serde-derive-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-stdweb-internal-macros-0.2
  (package
    (name "rust-stdweb-internal-macros")
    (version "0.2.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "stdweb-internal-macros" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "049fq8fl5ny9l5if2qv7kxwng7g6ns95h4fbm3zx360dmpv5zyjq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-syn" ,rust-syn-1)
         ("rust-sha1" ,rust-sha1-0.6)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-serde-derive" ,rust-serde-derive-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-base-x" ,rust-base-x-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-stdweb-internal-runtime-0.1
  (package
    (name "rust-stdweb-internal-runtime")
    (version "0.1.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "stdweb-internal-runtime" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1h0nkppb4r8dbrbms2hw9n5xdcs392m0r5hj3b6lsx3h6fx02dr1"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-strider-0.2
  (package
    (name "rust-strider")
    (version "0.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "strider" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0000000000000000000000000000000000000000000000000000"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-zellij-tile" ,rust-zellij-tile-0.40)
         ("rust-unicode-width" ,rust-unicode-width-0.1)
         ("rust-strip-ansi-escapes"
          ,rust-strip-ansi-escapes-0.1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-pretty-bytes" ,rust-pretty-bytes-0.2)
         ("rust-ignore" ,rust-ignore-0.4)
         ("rust-fuzzy-matcher" ,rust-fuzzy-matcher-0.3)
         ("rust-colored" ,rust-colored-2)
         ("rust-ansi-term" ,rust-ansi-term-0.12))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-strip-ansi-escapes-0.1
  (package
    (name "rust-strip-ansi-escapes")
    (version "0.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "strip-ansi-escapes" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1n36ly9vxb1wr5q76i7995xr7c0pb1pc8g7a3a3n47vwrwwvn701"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-vte" ,rust-vte-0.10))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-strsim-0.10
  (package
    (name "rust-strsim")
    (version "0.10.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "strsim" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "08s69r4rcrahwnickvi0kq49z524ci50capybln83mg6b473qivk"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-strum-0.20
  (package
    (name "rust-strum")
    (version "0.20.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "strum" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0p5cslmdnz261kiwmm4h7qsmv9bh83r0f9lq6f2z2mxsnl4wa63k"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-strum-macros-0.20
  (package
    (name "rust-strum-macros")
    (version "0.20.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "strum_macros" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0j9ikvxlqzr667ghc045qkpwprjgcfmzgagln7maw4jigawcd2zf"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-syn" ,rust-syn-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-heck" ,rust-heck-0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-subtle-2
  (package
    (name "rust-subtle")
    (version "2.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "subtle" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "00b6jzh9gzb0h9n25g06nqr90z3xzqppfhhb260s1hjhh4pg7pkb"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-suggest-0.4
  (package
    (name "rust-suggest")
    (version "0.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "suggest" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0nb6axl4i58g7k0q3p3bg6m363aw6qnqdg31y5c8b43x6bbd0n15"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-lev-distance" ,rust-lev-distance-0.1)
         ("rust-clap" ,rust-clap-3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-supports-color-2
  (package
    (name "rust-supports-color")
    (version "2.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "supports-color" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0m5kayz225f23k5jyjin82sfkrqhfdq3j72ianafkazz9cbyfl29"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-is-ci" ,rust-is-ci-1)
         ("rust-is-terminal" ,rust-is-terminal-0.4))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-supports-hyperlinks-2
  (package
    (name "rust-supports-hyperlinks")
    (version "2.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "supports-hyperlinks" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0g93nh1db3f9lyd0ry35bqjrxkg6sbysn36x9hgd9m5h5rlk2hpq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-is-terminal" ,rust-is-terminal-0.4))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-supports-unicode-2
  (package
    (name "rust-supports-unicode")
    (version "2.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "supports-unicode" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1xxscsdjmdp7i3ikqnnivfn4hnpy4gp9as4hshgd4pdb82r2qv2b"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-is-terminal" ,rust-is-terminal-0.4))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-surf-2
  (package
    (name "rust-surf")
    (version "2.3.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "surf" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1mwd0fj0pcdd1q3qp4r045znf0gnvsq1s0pzxlnrhl83npk1m2vi"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde-json" ,rust-serde-json-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-pin-project-lite"
          ,rust-pin-project-lite-0.2)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-mime-guess" ,rust-mime-guess-2)
         ("rust-log" ,rust-log-0.4)
         ("rust-http-types" ,rust-http-types-2)
         ("rust-http-client" ,rust-http-client-6)
         ("rust-getrandom" ,rust-getrandom-0.2)
         ("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-async-trait" ,rust-async-trait-0.1)
         ("rust-async-std" ,rust-async-std-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-syn-1
  (package
    (name "rust-syn")
    (version "1.0.96")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "syn" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1gqymymz4202nfj76dkhr177wmcidch580vzf6w3qi943qjxsj07"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-unicode-ident" ,rust-unicode-ident-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-syn-2
  (package
    (name "rust-syn")
    (version "2.0.15")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "syn" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "08n8c235bj7f86a5jg561s5zjfijdn8jw6ih2im7xxb0iczcykx3"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-unicode-ident" ,rust-unicode-ident-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-sysinfo-0.22
  (package
    (name "rust-sysinfo")
    (version "0.22.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "sysinfo" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0hsx8pl4yx4fkjx609pqhscklvhmr2ljqrhs8lr778h6ffqgl6vz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-winapi" ,rust-winapi-0.3)
         ("rust-rayon" ,rust-rayon-1)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-ntapi" ,rust-ntapi-0.3)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-core-foundation-sys"
          ,rust-core-foundation-sys-0.8)
         ("rust-cfg-if" ,rust-cfg-if-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-tab-bar-0.1
  (package
    (name "rust-tab-bar")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tab-bar" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0000000000000000000000000000000000000000000000000000"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-zellij-tile-utils"
          ,rust-zellij-tile-utils-0.40)
         ("rust-zellij-tile" ,rust-zellij-tile-0.40)
         ("rust-unicode-width" ,rust-unicode-width-0.1)
         ("rust-colored" ,rust-colored-2)
         ("rust-ansi-term" ,rust-ansi-term-0.12))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-target-lexicon-0.12
  (package
    (name "rust-target-lexicon")
    (version "0.12.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "target-lexicon" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0bgiahxqlk0sy3yiz4idzwg2r7gjc3grbyqrwpq9879vhpvd044l"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-tempfile-3
  (package
    (name "rust-tempfile")
    (version "3.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tempfile" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1r3rdp66f7w075mz6blh244syr3h0lbm07ippn7xrbgfxbs1xnsw"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-winapi" ,rust-winapi-0.3)
         ("rust-remove-dir-all" ,rust-remove-dir-all-0.5)
         ("rust-redox-syscall" ,rust-redox-syscall-0.2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-fastrand" ,rust-fastrand-1)
         ("rust-cfg-if" ,rust-cfg-if-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-termcolor-1
  (package
    (name "rust-termcolor")
    (version "1.1.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "termcolor" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0mbpflskhnz3jf312k50vn0hqbql8ga2rk0k79pkgchip4q4vcms"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-winapi-util" ,rust-winapi-util-0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-terminal-size-0.1
  (package
    (name "rust-terminal-size")
    (version "0.1.17")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "terminal_size" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1pq60ng1a7fjp597ifk1cqlz8fv9raz9xihddld1m1pfdia1lg33"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-winapi" ,rust-winapi-0.3)
         ("rust-libc" ,rust-libc-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-terminfo-0.7
  (package
    (name "rust-terminfo")
    (version "0.7.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "terminfo" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0zkln56bsixjad6rsfy9mm15d9ygm89i63cn3gn685hjwrvik5vn"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-phf-codegen" ,rust-phf-codegen-0.8)
         ("rust-phf" ,rust-phf-0.8)
         ("rust-nom" ,rust-nom-5)
         ("rust-fnv" ,rust-fnv-1)
         ("rust-dirs" ,rust-dirs-2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-termios-0.3
  (package
    (name "rust-termios")
    (version "0.3.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "termios" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0sxcs0g00538jqh5xbdqakkzijadr8nj7zmip0c7jz3k83vmn721"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-termwiz-0.20
  (package
    (name "rust-termwiz")
    (version "0.20.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "termwiz" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1yj80sli95wcw0im2iic9h7mx20hms3f9shxk7jarjqgl5waj2cm"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-winapi" ,rust-winapi-0.3)
         ("rust-wezterm-dynamic"
          ,rust-wezterm-dynamic-0.1)
         ("rust-wezterm-color-types"
          ,rust-wezterm-color-types-0.2)
         ("rust-wezterm-bidi" ,rust-wezterm-bidi-0.2)
         ("rust-vtparse" ,rust-vtparse-0.6)
         ("rust-unicode-segmentation"
          ,rust-unicode-segmentation-1)
         ("rust-ucd-trie" ,rust-ucd-trie-0.1)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-termios" ,rust-termios-0.3)
         ("rust-terminfo" ,rust-terminfo-0.7)
         ("rust-siphasher" ,rust-siphasher-0.3)
         ("rust-signal-hook" ,rust-signal-hook-0.1)
         ("rust-sha2" ,rust-sha2-0.9)
         ("rust-semver" ,rust-semver-0.11)
         ("rust-regex" ,rust-regex-1)
         ("rust-phf" ,rust-phf-0.10)
         ("rust-pest-derive" ,rust-pest-derive-2)
         ("rust-pest" ,rust-pest-2)
         ("rust-ordered-float" ,rust-ordered-float-3)
         ("rust-num-traits" ,rust-num-traits-0.2)
         ("rust-num-derive" ,rust-num-derive-0.3)
         ("rust-nix" ,rust-nix-0.24)
         ("rust-memmem" ,rust-memmem-0.1)
         ("rust-log" ,rust-log-0.4)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-hex" ,rust-hex-0.4)
         ("rust-fixedbitset" ,rust-fixedbitset-0.4)
         ("rust-finl-unicode" ,rust-finl-unicode-1)
         ("rust-filedescriptor" ,rust-filedescriptor-0.8)
         ("rust-bitflags" ,rust-bitflags-1)
         ("rust-base64" ,rust-base64-0.21)
         ("rust-anyhow" ,rust-anyhow-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-textwrap-0.15
  (package
    (name "rust-textwrap")
    (version "0.15.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "textwrap" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1yw513k61lfiwgqrfvsjw1a5wpvm0azhpjr2kr0jhnq9c56is55i"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-unicode-width" ,rust-unicode-width-0.1)
         ("rust-unicode-linebreak"
          ,rust-unicode-linebreak-0.1)
         ("rust-smawk" ,rust-smawk-0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-thiserror-1
  (package
    (name "rust-thiserror")
    (version "1.0.40")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "thiserror" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1b7bdhriasdsr99y39d50jz995xaz9sw3hsbb6z9kp6q9cqrm34p"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-thiserror-impl" ,rust-thiserror-impl-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-thiserror-impl-1
  (package
    (name "rust-thiserror-impl")
    (version "1.0.40")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "thiserror-impl" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "17sn41kyimc6s983aypkk6a45pcyrkbkvrw6rp407n5hqm16ligr"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-syn" ,rust-syn-2)
         ("rust-quote" ,rust-quote-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-thread-id-4
  (package
    (name "rust-thread-id")
    (version "4.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "thread-id" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0zvikdngp0950hi0jgiipr8l36rskk1wk7pc8cd43xr3g5if1psz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-winapi" ,rust-winapi-0.3)
         ("rust-redox-syscall" ,rust-redox-syscall-0.2)
         ("rust-libc" ,rust-libc-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-thread-local-1
  (package
    (name "rust-thread-local")
    (version "1.1.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "thread_local" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0lp19jdgvp5m4l60cgxdnl00yw1hlqy8gcywg9bddwng9h36zp9z"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-once-cell" ,rust-once-cell-1)
         ("rust-cfg-if" ,rust-cfg-if-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-time-0.1
  (package
    (name "rust-time")
    (version "0.1.44")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "time" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0m9jwy2pcmk232r3b9r80fs12mkckfjffjha4qfaxcdq9a8ydfbd"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-winapi" ,rust-winapi-0.3)
         ("rust-wasi" ,rust-wasi-0.10)
         ("rust-libc" ,rust-libc-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-time-0.2
  (package
    (name "rust-time")
    (version "0.2.27")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "time" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0hm209d078jfgxzjhi5xqim64q31rlj1h70zz57qbmpbirzsjlj7"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-winapi" ,rust-winapi-0.3)
         ("rust-version-check" ,rust-version-check-0.9)
         ("rust-time-macros" ,rust-time-macros-0.1)
         ("rust-stdweb" ,rust-stdweb-0.4)
         ("rust-standback" ,rust-standback-0.2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-const-fn" ,rust-const-fn-0.4))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-time-macros-0.1
  (package
    (name "rust-time-macros")
    (version "0.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "time-macros" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1wg24yxpxcfmim6dgblrf8p321m7cyxpdivzvp8bcb7i4rp9qzlm"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-time-macros-impl"
          ,rust-time-macros-impl-0.1)
         ("rust-proc-macro-hack"
          ,rust-proc-macro-hack-0.5))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-time-macros-impl-0.1
  (package
    (name "rust-time-macros-impl")
    (version "0.1.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "time-macros-impl" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0bs8xc3qbndk4nw6vwnmh5bwail6vwji4hd1aqzly6a33cd18g7x"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-syn" ,rust-syn-1)
         ("rust-standback" ,rust-standback-0.2)
         ("rust-quote" ,rust-quote-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-proc-macro-hack"
          ,rust-proc-macro-hack-0.5))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-tinyvec-1
  (package
    (name "rust-tinyvec")
    (version "1.6.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tinyvec" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0l6bl2h62a5m44jdnpn7lmj14rd44via8180i7121fvm73mmrk47"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-tinyvec-macros" ,rust-tinyvec-macros-0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-tinyvec-macros-0.1
  (package
    (name "rust-tinyvec-macros")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tinyvec_macros" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0p5zvgbas5nh403fbxica819mf3g83n8g2hzpfazfr56w6klv9yd"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-to-method-1
  (package
    (name "rust-to-method")
    (version "1.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "to_method" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1s72l06fnb5kv6vm5ds0lilg1dyciyyis09ypi5kij0mrbpcxi67"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-toml-0.5
  (package
    (name "rust-toml")
    (version "0.5.10")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "toml" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "13srgxa0d92k8vdjyw8cyyax1pvi66smlyq1s7cs8s7891kwfcqk"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde" ,rust-serde-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-tracing-0.1
  (package
    (name "rust-tracing")
    (version "0.1.35")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tracing" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0q01jipgap393wr0s7ijm01nnmhqbcxk9q4f5ajl978blqdf6054"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-tracing-core" ,rust-tracing-core-0.1)
         ("rust-tracing-attributes"
          ,rust-tracing-attributes-0.1)
         ("rust-pin-project-lite"
          ,rust-pin-project-lite-0.2)
         ("rust-log" ,rust-log-0.4)
         ("rust-cfg-if" ,rust-cfg-if-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-tracing-attributes-0.1
  (package
    (name "rust-tracing-attributes")
    (version "0.1.21")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tracing-attributes" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0z2bjwkh0azvxw0fqcn36iy7r33wgaq559xp3n5gk6blav9qlsyc"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-syn" ,rust-syn-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-tracing-core-0.1
  (package
    (name "rust-tracing-core")
    (version "0.1.27")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tracing-core" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "08fr2y0nm0as3ar22xw3qspwjfbx1a4byzp8wmf9d93qi1dmj2bp"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-once-cell" ,rust-once-cell-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-tracing-futures-0.2
  (package
    (name "rust-tracing-futures")
    (version "0.2.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tracing-futures" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1wimg0iwa2ldq7xv98lvivvf3q9ykfminig8r1bs0ig22np9bl4p"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-tracing" ,rust-tracing-0.1)
         ("rust-pin-project" ,rust-pin-project-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-typemap-ors-1
  (package
    (name "rust-typemap-ors")
    (version "1.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "typemap-ors" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0rw8lbbh8aarfacyz133p0pqq1gj96fypk2c3s7x2bgh0yvj9356"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-unsafe-any-ors" ,rust-unsafe-any-ors-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-typenum-1
  (package
    (name "rust-typenum")
    (version "1.15.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "typenum" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "11yrvz1vd43gqv738yw1v75rzngjbs7iwcgzjy3cq5ywkv2imy6w"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-typetag-0.1
  (package
    (name "rust-typetag")
    (version "0.1.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "typetag" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "12jva00k063gb48bvx0p0ixwbq1l48411disynzvah92bd65d020"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-typetag-impl" ,rust-typetag-impl-0.1)
         ("rust-serde" ,rust-serde-1)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-inventory" ,rust-inventory-0.2)
         ("rust-erased-serde" ,rust-erased-serde-0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-typetag-impl-0.1
  (package
    (name "rust-typetag-impl")
    (version "0.1.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "typetag-impl" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "03lw15ad39bgr4m6fmr5b9lb4wapkcfsnfxsbz0362635iw4f0g6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-syn" ,rust-syn-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-ucd-trie-0.1
  (package
    (name "rust-ucd-trie")
    (version "0.1.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ucd-trie" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "072cblf8v3wzyaz3lhbpzgil4s03dpzg1ppy3gqx2l4v622y3pjn"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-unicase-2
  (package
    (name "rust-unicase")
    (version "2.6.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unicase" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1xmlbink4ycgxrkjspp0mf7pghcx4m7vxq7fpfm04ikr2zk7pwsh"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-version-check" ,rust-version-check-0.9))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-unicode-bidi-0.3
  (package
    (name "rust-unicode-bidi")
    (version "0.3.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unicode-bidi" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "14p95n9kw9p7psp0vsp0j9yfkfg6sn1rlnymvmwmya0x60l736q9"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-unicode-ident-1
  (package
    (name "rust-unicode-ident")
    (version "1.0.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unicode-ident" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "131niycgp77aiwvgjdyh47389xfnb7fmlc8ybrxys8v0a0kgxljv"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-unicode-linebreak-0.1
  (package
    (name "rust-unicode-linebreak")
    (version "0.1.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unicode-linebreak" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0grq6bsn967q4vpifld53s7a140nlmpq5vy8ghgr73f4n2mdqlis"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-regex" ,rust-regex-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-unicode-normalization-0.1
  (package
    (name "rust-unicode-normalization")
    (version "0.1.19")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unicode-normalization" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1yabhmg8zlcksda3ajly9hpbzqgbhknxwch8dwkfkaa1569r0ifm"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-tinyvec" ,rust-tinyvec-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-unicode-segmentation-1
  (package
    (name "rust-unicode-segmentation")
    (version "1.9.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unicode-segmentation" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "16gxxda9aya0arcqs9aa9lb31b3i54i34dmyqi6j5xkpszsj123y"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-unicode-width-0.1
  (package
    (name "rust-unicode-width")
    (version "0.1.10")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unicode-width" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "12vc3wv0qwg8rzcgb9bhaf5119dlmd6lmkhbfy1zfls6n7jx3vf0"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-unicode-xid-0.2
  (package
    (name "rust-unicode-xid")
    (version "0.2.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unicode-xid" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "131dfzf7d8fsr1ivch34x42c2d1ik5ig3g78brxncnn0r1sdyqpr"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-universal-hash-0.4
  (package
    (name "rust-universal-hash")
    (version "0.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "universal-hash" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "01av09i0rqcl8f0xgvn2g07kzyafgbiwdhkfwq0m14kyd67lw8cz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-subtle" ,rust-subtle-2)
         ("rust-generic-array" ,rust-generic-array-0.14))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-unsafe-any-ors-1
  (package
    (name "rust-unsafe-any-ors")
    (version "1.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unsafe-any-ors" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1bf0hxfhb3gh9hy8pw6l0jaqjprzn9w1vnfph2b2sdk50v9h78z0"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-destructure-traitobject"
          ,rust-destructure-traitobject-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-url-2
  (package
    (name "rust-url")
    (version "2.2.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "url" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "132pzpvfvpw33gjlzqd55n5iag9qddzffq8qbp1myfykna1w61x5"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde" ,rust-serde-1)
         ("rust-percent-encoding"
          ,rust-percent-encoding-2)
         ("rust-matches" ,rust-matches-0.1)
         ("rust-idna" ,rust-idna-0.2)
         ("rust-form-urlencoded" ,rust-form-urlencoded-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-utf8parse-0.2
  (package
    (name "rust-utf8parse")
    (version "0.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "utf8parse" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0wjkvy22cxg023vkmvq2wwkgqyqam0d4pjld3m13blfg594lnvlk"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-uuid-1
  (package
    (name "rust-uuid")
    (version "1.7.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uuid" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0aivp5ys7sg2izlj2sn6rr8p43vdcwg64naj8n0kqbd15iqcj37h"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde" ,rust-serde-1)
         ("rust-getrandom" ,rust-getrandom-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-value-bag-1
  (package
    (name "rust-value-bag")
    (version "1.0.0-alpha.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "value-bag" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0mgc2vlqikx16gabp4ghbm3fs773kxvwjmrn57rydxs92a6vf292"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-version-check" ,rust-version-check-0.9)
         ("rust-ctor" ,rust-ctor-0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-vcpkg-0.2
  (package
    (name "rust-vcpkg")
    (version "0.2.15")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "vcpkg" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "09i4nf5y8lig6xgj3f7fyrvzd3nlaw4znrihw8psidvv5yk4xkdc"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-version-check-0.9
  (package
    (name "rust-version-check")
    (version "0.9.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "version_check" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0gs8grwdlgh0xq660d7wr80x14vxbizmd8dbp29p2pdncx8lp1s9"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-vte-0.10
  (package
    (name "rust-vte")
    (version "0.10.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "vte" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "10srmy9ssircrwsb5lpx3fbhx71460j77kvz0krz38jcmf9fdg3c"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-vte-generate-state-changes"
          ,rust-vte-generate-state-changes-0.1)
         ("rust-utf8parse" ,rust-utf8parse-0.2)
         ("rust-arrayvec" ,rust-arrayvec-0.5))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-vte-0.11
  (package
    (name "rust-vte")
    (version "0.11.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "vte" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0iah6qvi8r6a1lslcwjg2g0jnczz72f3cvr3ihb2vv6j5b0j3bhs"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-vte-generate-state-changes"
          ,rust-vte-generate-state-changes-0.1)
         ("rust-utf8parse" ,rust-utf8parse-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-vte-generate-state-changes-0.1
  (package
    (name "rust-vte-generate-state-changes")
    (version "0.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "vte_generate_state_changes" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1zs5q766q7jmc80c5c80gpzy4qpg5lnydf94mgdzrpy7h5q82myj"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-quote" ,rust-quote-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-vtparse-0.6
  (package
    (name "rust-vtparse")
    (version "0.6.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "vtparse" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1l5yz9650zhkaffxn28cvfys7plcw2wd6drajyf41pshn37jm6vd"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-utf8parse" ,rust-utf8parse-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-waker-fn-1
  (package
    (name "rust-waker-fn")
    (version "1.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "waker-fn" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1jpfiis0frk2b36krqvk8264kgxk2dyhfzjsr8g3wah1nii2qnwx"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-walkdir-2
  (package
    (name "rust-walkdir")
    (version "2.3.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "walkdir" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "16768hy32kcvghq7v4ci8llfjvdiwrwg6sj9nzcdiisnv9699prn"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-winapi-util" ,rust-winapi-util-0.1)
         ("rust-same-file" ,rust-same-file-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasi-0.9
  (package
    (name "rust-wasi")
    (version "0.9.0+wasi-snapshot-preview1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasi" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "06g5v3vrdapfzvfq662cij7v8a1flwr2my45nnncdv2galrdzkfc"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasi-0.10
  (package
    (name "rust-wasi")
    (version "0.10.0+wasi-snapshot-preview1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasi" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "07y3l8mzfzzz4cj09c8y90yak4hpsi9g7pllyzpr6xvwrabka50s"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasi-0.11
  (package
    (name "rust-wasi")
    (version "0.11.0+wasi-snapshot-preview1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasi" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "08z4hxwkpdpalxjps1ai9y7ihin26y9f476i53dv98v45gkqg3cw"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasm-bindgen-0.2
  (package
    (name "rust-wasm-bindgen")
    (version "0.2.87")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasm-bindgen" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0hm3k42gcnrps2jh339h186scx1radqy1w7v1zwb333dncmaf1kp"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-wasm-bindgen-macro"
          ,rust-wasm-bindgen-macro-0.2)
         ("rust-cfg-if" ,rust-cfg-if-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasm-bindgen-backend-0.2
  (package
    (name "rust-wasm-bindgen-backend")
    (version "0.2.87")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasm-bindgen-backend" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1gcsh3bjxhw3cirmin45107pcsnn0ymhkxg6bxg65s8hqp9vdwjy"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-wasm-bindgen-shared"
          ,rust-wasm-bindgen-shared-0.2)
         ("rust-syn" ,rust-syn-2)
         ("rust-quote" ,rust-quote-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-log" ,rust-log-0.4)
         ("rust-bumpalo" ,rust-bumpalo-3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasm-bindgen-downcast-0.1
  (package
    (name "rust-wasm-bindgen-downcast")
    (version "0.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasm-bindgen-downcast" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0h23lhqjrrqvi5fhm4wf7r0gdvariyk6p5f0w5y6xjmw8dnh5b2x"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-wasm-bindgen-downcast-macros"
          ,rust-wasm-bindgen-downcast-macros-0.1)
         ("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-js-sys" ,rust-js-sys-0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasm-bindgen-downcast-macros-0.1
  (package
    (name "rust-wasm-bindgen-downcast-macros")
    (version "0.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri
               "wasm-bindgen-downcast-macros"
               version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "13zk8l5qligf53ah2my2gqn15z61wd25s18qy7zcxkn7hzx0q0n5"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-syn" ,rust-syn-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasm-bindgen-futures-0.4
  (package
    (name "rust-wasm-bindgen-futures")
    (version "0.4.31")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasm-bindgen-futures" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "07zk2d6wx2ibnz2qfpmr540jrbnj2cjglmvycn68liik2zn9r6ny"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-web-sys" ,rust-web-sys-0.3)
         ("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2)
         ("rust-js-sys" ,rust-js-sys-0.3)
         ("rust-cfg-if" ,rust-cfg-if-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasm-bindgen-macro-0.2
  (package
    (name "rust-wasm-bindgen-macro")
    (version "0.2.87")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasm-bindgen-macro" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "07cg0b6zkcxa1yg1n10h62paid59s9zr8yss214bv8w2b7jrbr6y"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-wasm-bindgen-macro-support"
          ,rust-wasm-bindgen-macro-support-0.2)
         ("rust-quote" ,rust-quote-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasm-bindgen-macro-support-0.2
  (package
    (name "rust-wasm-bindgen-macro-support")
    (version "0.2.87")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasm-bindgen-macro-support" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0yqc46pr6mlgb9bsnfdnd50qvsqnrz8g5243fnaz0rb7lhc1ns2l"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-wasm-bindgen-shared"
          ,rust-wasm-bindgen-shared-0.2)
         ("rust-wasm-bindgen-backend"
          ,rust-wasm-bindgen-backend-0.2)
         ("rust-syn" ,rust-syn-2)
         ("rust-quote" ,rust-quote-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasm-bindgen-shared-0.2
  (package
    (name "rust-wasm-bindgen-shared")
    (version "0.2.87")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasm-bindgen-shared" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "18bmjwvfyhvlq49nzw6mgiyx4ys350vps4cmx5gvzckh91dd0sna"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasm-encoder-0.13
  (package
    (name "rust-wasm-encoder")
    (version "0.13.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasm-encoder" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1skmk8v935lwj1mh3dsgr1ridnr11r38jxc97npddzx5cxrc3w1i"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-leb128" ,rust-leb128-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasmer-3
  (package
    (name "rust-wasmer")
    (version "3.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasmer" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "140gkya1vg7g3fj4hvv49ydrxf8w62ph2wnw0nw0q8h12z9gc2l4"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-winapi" ,rust-winapi-0.3)
         ("rust-wat" ,rust-wat-1)
         ("rust-wasmer-vm" ,rust-wasmer-vm-3)
         ("rust-wasmer-types" ,rust-wasmer-types-3)
         ("rust-wasmer-derive" ,rust-wasmer-derive-3)
         ("rust-wasmer-compiler-singlepass"
          ,rust-wasmer-compiler-singlepass-3)
         ("rust-wasmer-compiler-cranelift"
          ,rust-wasmer-compiler-cranelift-3)
         ("rust-wasmer-compiler" ,rust-wasmer-compiler-3)
         ("rust-wasm-bindgen-downcast"
          ,rust-wasm-bindgen-downcast-0.1)
         ("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-target-lexicon" ,rust-target-lexicon-0.12)
         ("rust-serde-wasm-bindgen"
          ,rust-serde-wasm-bindgen-0.4)
         ("rust-serde" ,rust-serde-1)
         ("rust-more-asserts" ,rust-more-asserts-0.2)
         ("rust-js-sys" ,rust-js-sys-0.3)
         ("rust-indexmap" ,rust-indexmap-1)
         ("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-bytes" ,rust-bytes-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasmer-compiler-3
  (package
    (name "rust-wasmer-compiler")
    (version "3.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasmer-compiler" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1ig04c4r4kxilsg4kf6ic0w4ff9pfy0yc16bh1rygb5apscanvxq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-winapi" ,rust-winapi-0.3)
         ("rust-wasmparser" ,rust-wasmparser-0.83)
         ("rust-wasmer-vm" ,rust-wasmer-vm-3)
         ("rust-wasmer-types" ,rust-wasmer-types-3)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-smallvec" ,rust-smallvec-1)
         ("rust-rustc-demangle" ,rust-rustc-demangle-0.1)
         ("rust-region" ,rust-region-3)
         ("rust-more-asserts" ,rust-more-asserts-0.2)
         ("rust-memmap2" ,rust-memmap2-0.5)
         ("rust-leb128" ,rust-leb128-0.2)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-enumset" ,rust-enumset-1)
         ("rust-enum-iterator" ,rust-enum-iterator-0.7)
         ("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-backtrace" ,rust-backtrace-0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasmer-compiler-cranelift-3
  (package
    (name "rust-wasmer-compiler-cranelift")
    (version "3.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasmer-compiler-cranelift" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "13z5d1ifkns9djqpblnjkzd96jsjzbbnns611ma8jj68kxifyph1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-wasmer-types" ,rust-wasmer-types-3)
         ("rust-wasmer-compiler" ,rust-wasmer-compiler-3)
         ("rust-tracing" ,rust-tracing-0.1)
         ("rust-target-lexicon" ,rust-target-lexicon-0.12)
         ("rust-smallvec" ,rust-smallvec-1)
         ("rust-rayon" ,rust-rayon-1)
         ("rust-more-asserts" ,rust-more-asserts-0.2)
         ("rust-gimli" ,rust-gimli-0.26)
         ("rust-cranelift-frontend"
          ,rust-cranelift-frontend-0.86)
         ("rust-cranelift-entity"
          ,rust-cranelift-entity-0.86)
         ("rust-cranelift-codegen"
          ,rust-cranelift-codegen-0.86))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasmer-compiler-singlepass-3
  (package
    (name "rust-wasmer-compiler-singlepass")
    (version "3.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasmer-compiler-singlepass" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0p15imhw2bi3s9wxkc7iq7mzxp20h3s319788y8z7mcjq763bqh7"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-wasmer-types" ,rust-wasmer-types-3)
         ("rust-wasmer-compiler" ,rust-wasmer-compiler-3)
         ("rust-smallvec" ,rust-smallvec-1)
         ("rust-rayon" ,rust-rayon-1)
         ("rust-more-asserts" ,rust-more-asserts-0.2)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-gimli" ,rust-gimli-0.26)
         ("rust-enumset" ,rust-enumset-1)
         ("rust-dynasmrt" ,rust-dynasmrt-1)
         ("rust-dynasm" ,rust-dynasm-1)
         ("rust-byteorder" ,rust-byteorder-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasmer-derive-3
  (package
    (name "rust-wasmer-derive")
    (version "3.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasmer-derive" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1ab3d852bj152bvmz144f30zfwwv94dgw29sbdy3vkfgq6vpgx8z"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-syn" ,rust-syn-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-proc-macro-error"
          ,rust-proc-macro-error-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasmer-types-3
  (package
    (name "rust-wasmer-types")
    (version "3.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasmer-types" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0wy4n4y0c1mccbiirl522n2ciwniwxh4a2hbpvivlslnvbwh15lb"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-thiserror" ,rust-thiserror-1)
         ("rust-target-lexicon" ,rust-target-lexicon-0.12)
         ("rust-rkyv" ,rust-rkyv-0.7)
         ("rust-more-asserts" ,rust-more-asserts-0.2)
         ("rust-indexmap" ,rust-indexmap-1)
         ("rust-enumset" ,rust-enumset-1)
         ("rust-enum-iterator" ,rust-enum-iterator-0.7))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasmer-vbus-3
  (package
    (name "rust-wasmer-vbus")
    (version "3.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasmer-vbus" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1nybkmk9m0x1gxvg7v05a15ryq0797gmz8z39l46i77hp5v2zd3j"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-wasmer-vfs" ,rust-wasmer-vfs-3)
         ("rust-thiserror" ,rust-thiserror-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasmer-vfs-3
  (package
    (name "rust-wasmer-vfs")
    (version "3.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasmer-vfs" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "04z4p56py79197vbqbbq5mnaw72ng9jr98f89znlmmh36ljbvgrl"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-tracing" ,rust-tracing-0.1)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-libc" ,rust-libc-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasmer-vm-3
  (package
    (name "rust-wasmer-vm")
    (version "3.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasmer-vm" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1866ld6p9qd5p6pn8790afajhz89cl053ds572v6qgh019zqmilz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-winapi" ,rust-winapi-0.3)
         ("rust-wasmer-types" ,rust-wasmer-types-3)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-scopeguard" ,rust-scopeguard-1)
         ("rust-region" ,rust-region-3)
         ("rust-more-asserts" ,rust-more-asserts-0.2)
         ("rust-memoffset" ,rust-memoffset-0.6)
         ("rust-mach" ,rust-mach-0.3)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-indexmap" ,rust-indexmap-1)
         ("rust-enum-iterator" ,rust-enum-iterator-0.7)
         ("rust-corosensei" ,rust-corosensei-0.1)
         ("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-cc" ,rust-cc-1)
         ("rust-backtrace" ,rust-backtrace-0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasmer-vnet-3
  (package
    (name "rust-wasmer-vnet")
    (version "3.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasmer-vnet" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1hdlglp0qx8l51sj3hhjfi4g6vm7v2carpyw3f821inc90xzxi4v"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-wasmer-vfs" ,rust-wasmer-vfs-3)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-bytes" ,rust-bytes-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasmer-wasi-3
  (package
    (name "rust-wasmer-wasi")
    (version "3.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasmer-wasi" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "079hpk2fi8kaxa49sbvdnbsxgqb0ys8fxymcgnqq7nv3gkayr4z8"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-winapi" ,rust-winapi-0.3)
         ("rust-wasmer-wasi-types"
          ,rust-wasmer-wasi-types-3)
         ("rust-wasmer-wasi-local-networking"
          ,rust-wasmer-wasi-local-networking-3)
         ("rust-wasmer-vnet" ,rust-wasmer-vnet-3)
         ("rust-wasmer-vfs" ,rust-wasmer-vfs-3)
         ("rust-wasmer-vbus" ,rust-wasmer-vbus-3)
         ("rust-wasmer" ,rust-wasmer-3)
         ("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2)
         ("rust-tracing" ,rust-tracing-0.1)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-getrandom" ,rust-getrandom-0.2)
         ("rust-generational-arena"
          ,rust-generational-arena-0.2)
         ("rust-derivative" ,rust-derivative-2)
         ("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-bytes" ,rust-bytes-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasmer-wasi-local-networking-3
  (package
    (name "rust-wasmer-wasi-local-networking")
    (version "3.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri
               "wasmer-wasi-local-networking"
               version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0c574yawhh690irxhivrjafi6gpznxwyj9kgfx43hygfihnqhrdx"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-wasmer-vnet" ,rust-wasmer-vnet-3)
         ("rust-wasmer-vfs" ,rust-wasmer-vfs-3)
         ("rust-tracing" ,rust-tracing-0.1)
         ("rust-bytes" ,rust-bytes-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasmer-wasi-types-3
  (package
    (name "rust-wasmer-wasi-types")
    (version "3.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasmer-wasi-types" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1m3121d096lik4rj32kjzlr86w4gl1idd0qb27vjplk27k4dxbz1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-wasmer-wit-parser"
          ,rust-wasmer-wit-parser-0.1)
         ("rust-wasmer-wit-bindgen-rust"
          ,rust-wasmer-wit-bindgen-rust-0.1)
         ("rust-wasmer-wit-bindgen-gen-rust-wasm"
          ,rust-wasmer-wit-bindgen-gen-rust-wasm-0.1)
         ("rust-wasmer-wit-bindgen-gen-core"
          ,rust-wasmer-wit-bindgen-gen-core-0.1)
         ("rust-wasmer-types" ,rust-wasmer-types-3)
         ("rust-wasmer-derive" ,rust-wasmer-derive-3)
         ("rust-wasmer" ,rust-wasmer-3)
         ("rust-time" ,rust-time-0.2)
         ("rust-byteorder" ,rust-byteorder-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasmer-wit-bindgen-gen-core-0.1
  (package
    (name "rust-wasmer-wit-bindgen-gen-core")
    (version "0.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasmer-wit-bindgen-gen-core" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1zjl8224fdbsxxjkn26jjn1y4gq90gkc1hhx2mg1zmp5bazab2pz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-wasmer-wit-parser"
          ,rust-wasmer-wit-parser-0.1)
         ("rust-anyhow" ,rust-anyhow-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasmer-wit-bindgen-gen-rust-0.1
  (package
    (name "rust-wasmer-wit-bindgen-gen-rust")
    (version "0.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasmer-wit-bindgen-gen-rust" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0frwkhhn1r0lq6wb12p581jgrdd98cj336yc03qjp1498mycx2s3"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-wasmer-wit-bindgen-gen-core"
          ,rust-wasmer-wit-bindgen-gen-core-0.1)
         ("rust-heck" ,rust-heck-0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasmer-wit-bindgen-gen-rust-wasm-0.1
  (package
    (name "rust-wasmer-wit-bindgen-gen-rust-wasm")
    (version "0.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri
               "wasmer-wit-bindgen-gen-rust-wasm"
               version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1z5swyz3cksn4688647p3pgxd59gbrf8l59yw501hngfrxl52psh"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-wasmer-wit-bindgen-gen-rust"
          ,rust-wasmer-wit-bindgen-gen-rust-0.1)
         ("rust-wasmer-wit-bindgen-gen-core"
          ,rust-wasmer-wit-bindgen-gen-core-0.1)
         ("rust-heck" ,rust-heck-0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasmer-wit-bindgen-rust-0.1
  (package
    (name "rust-wasmer-wit-bindgen-rust")
    (version "0.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasmer-wit-bindgen-rust" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "134iik2psvkfa035bfn39qxx36yv454mv70dnywslx0z4zqlg1wn"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-wasmer-wit-bindgen-rust-impl"
          ,rust-wasmer-wit-bindgen-rust-impl-0.1)
         ("rust-bitflags" ,rust-bitflags-1)
         ("rust-async-trait" ,rust-async-trait-0.1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasmer-wit-bindgen-rust-impl-0.1
  (package
    (name "rust-wasmer-wit-bindgen-rust-impl")
    (version "0.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri
               "wasmer-wit-bindgen-rust-impl"
               version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "11bv0ny7nypw97b38rhbfpkm3gfz2csd25xhf2c13llbs00gw9mx"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-wasmer-wit-bindgen-gen-rust-wasm"
          ,rust-wasmer-wit-bindgen-gen-rust-wasm-0.1)
         ("rust-wasmer-wit-bindgen-gen-core"
          ,rust-wasmer-wit-bindgen-gen-core-0.1)
         ("rust-syn" ,rust-syn-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasmer-wit-parser-0.1
  (package
    (name "rust-wasmer-wit-parser")
    (version "0.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasmer-wit-parser" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0njqg01s4qz23l1cs5rvhf3sdafkp413jqch6zms5s3b10arlv7l"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-unicode-xid" ,rust-unicode-xid-0.2)
         ("rust-unicode-normalization"
          ,rust-unicode-normalization-0.1)
         ("rust-pulldown-cmark" ,rust-pulldown-cmark-0.8)
         ("rust-id-arena" ,rust-id-arena-2)
         ("rust-anyhow" ,rust-anyhow-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wasmparser-0.83
  (package
    (name "rust-wasmparser")
    (version "0.83.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wasmparser" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0yhx2kq7da4sdglh1x1di4xxg33k7lwddpd3ri46bp9abk2xg3ki"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wast-42
  (package
    (name "rust-wast")
    (version "42.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wast" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1m4xzv5vd8ippd10jsqyyr198rdyjybdm57jvbq3z63gjwzv1p5s"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-wasm-encoder" ,rust-wasm-encoder-0.13)
         ("rust-unicode-width" ,rust-unicode-width-0.1)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-leb128" ,rust-leb128-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wat-1
  (package
    (name "rust-wat")
    (version "1.0.44")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wat" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1kv5r5qvmynfhqjz8zmhr9l1nrjjfdiw0js1r1k70lmc8avj0bxr"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-wast" ,rust-wast-42))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-web-sys-0.3
  (package
    (name "rust-web-sys")
    (version "0.3.58")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "web-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "140vnw410dx4cbvcs113lzbr5pmwq9fv3wjigpfzinjpxsz99v9g"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2)
         ("rust-js-sys" ,rust-js-sys-0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wepoll-ffi-0.1
  (package
    (name "rust-wepoll-ffi")
    (version "0.1.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wepoll-ffi" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1yxpkva08d5f6ih3b5hdb8h45mkz3jq3dh1bzjspfhy6qpnzshyp"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cc" ,rust-cc-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wezterm-bidi-0.2
  (package
    (name "rust-wezterm-bidi")
    (version "0.2.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wezterm-bidi" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0dkcwscvlwnv6lnagxfb08rcd21gfyrxbr7afcjaj3wvycn3hq0m"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-wezterm-dynamic"
          ,rust-wezterm-dynamic-0.1)
         ("rust-log" ,rust-log-0.4))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wezterm-color-types-0.2
  (package
    (name "rust-wezterm-color-types")
    (version "0.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wezterm-color-types" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0xvphmrqgg69v9l879xj5lq010z13f5ixi854ykmny6j7m47lvjc"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-wezterm-dynamic"
          ,rust-wezterm-dynamic-0.1)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-deltae" ,rust-deltae-0.3)
         ("rust-csscolorparser" ,rust-csscolorparser-0.6))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wezterm-dynamic-0.1
  (package
    (name "rust-wezterm-dynamic")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wezterm-dynamic" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1al8fmfr852m62mlcr0v2lg3a18icl2sv79zv7jnv9v0rk07hpm7"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-wezterm-dynamic-derive"
          ,rust-wezterm-dynamic-derive-0.1)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-strsim" ,rust-strsim-0.10)
         ("rust-ordered-float" ,rust-ordered-float-3)
         ("rust-log" ,rust-log-0.4))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-wezterm-dynamic-derive-0.1
  (package
    (name "rust-wezterm-dynamic-derive")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "wezterm-dynamic-derive" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1w07qf8njyq19nxi9vpshwprk00blhzg9ybis2rhfba433rmx7qc"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-syn" ,rust-syn-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-which-4
  (package
    (name "rust-which")
    (version "4.2.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "which" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1bi4gklz7qcw19z4d2a4c1wsq083zc2387745rvsidhkc57baksw"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-either" ,rust-either-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-winapi-0.3
  (package
    (name "rust-winapi")
    (version "0.3.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "winapi" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "06gl025x418lchw1wxj64ycr7gha83m44cjr5sarhynd9xkrm0sw"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-winapi-x86-64-pc-windows-gnu"
          ,rust-winapi-x86-64-pc-windows-gnu-0.4)
         ("rust-winapi-i686-pc-windows-gnu"
          ,rust-winapi-i686-pc-windows-gnu-0.4))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-winapi-i686-pc-windows-gnu-0.4
  (package
    (name "rust-winapi-i686-pc-windows-gnu")
    (version "0.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "winapi-i686-pc-windows-gnu" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1dmpa6mvcvzz16zg6d5vrfy4bxgg541wxrcip7cnshi06v38ffxc"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-winapi-util-0.1
  (package
    (name "rust-winapi-util")
    (version "0.1.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "winapi-util" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0y71bp7f6d536czj40dhqk0d55wfbbwqfp2ymqf1an5ibgl6rv3h"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-winapi" ,rust-winapi-0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-winapi-x86-64-pc-windows-gnu-0.4
  (package
    (name "rust-winapi-x86-64-pc-windows-gnu")
    (version "0.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri
               "winapi-x86_64-pc-windows-gnu"
               version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0gqq64czqb64kskjryj8isp62m2sgvx25yyj3kpc2myh85w24bki"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-sys-0.33
  (package
    (name "rust-windows-sys")
    (version "0.33.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0xdym5hgf2pp5lmfdjk4lynad99w4j02v9yzn6752a9ncsbb1ns3"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-windows-x86-64-msvc"
          ,rust-windows-x86-64-msvc-0.33)
         ("rust-windows-x86-64-gnu"
          ,rust-windows-x86-64-gnu-0.33)
         ("rust-windows-i686-msvc"
          ,rust-windows-i686-msvc-0.33)
         ("rust-windows-i686-gnu"
          ,rust-windows-i686-gnu-0.33)
         ("rust-windows-aarch64-msvc"
          ,rust-windows-aarch64-msvc-0.33))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-sys-0.45
  (package
    (name "rust-windows-sys")
    (version "0.45.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1l36bcqm4g89pknfp8r9rl1w4bn017q6a8qlx8viv0xjxzjkna3m"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-windows-targets"
          ,rust-windows-targets-0.42))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-sys-0.48
  (package
    (name "rust-windows-sys")
    (version "0.48.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1aan23v5gs7gya1lc46hqn9mdh8yph3fhxmhxlw36pn6pqc28zb7"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-windows-targets"
          ,rust-windows-targets-0.48))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-targets-0.42
  (package
    (name "rust-windows-targets")
    (version "0.42.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows-targets" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0wfhnib2fisxlx8c507dbmh97kgij4r6kcxdi0f9nk6l1k080lcf"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-windows-x86-64-msvc"
          ,rust-windows-x86-64-msvc-0.42)
         ("rust-windows-x86-64-gnullvm"
          ,rust-windows-x86-64-gnullvm-0.42)
         ("rust-windows-x86-64-gnu"
          ,rust-windows-x86-64-gnu-0.42)
         ("rust-windows-i686-msvc"
          ,rust-windows-i686-msvc-0.42)
         ("rust-windows-i686-gnu"
          ,rust-windows-i686-gnu-0.42)
         ("rust-windows-aarch64-msvc"
          ,rust-windows-aarch64-msvc-0.42)
         ("rust-windows-aarch64-gnullvm"
          ,rust-windows-aarch64-gnullvm-0.42))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-targets-0.48
  (package
    (name "rust-windows-targets")
    (version "0.48.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows-targets" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1mfzg94w0c8h4ya9sva7rra77f3iy1712af9b6bwg03wrpqbc7kv"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-windows-x86-64-msvc"
          ,rust-windows-x86-64-msvc-0.48)
         ("rust-windows-x86-64-gnullvm"
          ,rust-windows-x86-64-gnullvm-0.48)
         ("rust-windows-x86-64-gnu"
          ,rust-windows-x86-64-gnu-0.48)
         ("rust-windows-i686-msvc"
          ,rust-windows-i686-msvc-0.48)
         ("rust-windows-i686-gnu"
          ,rust-windows-i686-gnu-0.48)
         ("rust-windows-aarch64-msvc"
          ,rust-windows-aarch64-msvc-0.48)
         ("rust-windows-aarch64-gnullvm"
          ,rust-windows-aarch64-gnullvm-0.48))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-aarch64-gnullvm-0.42
  (package
    (name "rust-windows-aarch64-gnullvm")
    (version "0.42.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_aarch64_gnullvm" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1y4q0qmvl0lvp7syxvfykafvmwal5hrjb4fmv04bqs0bawc52yjr"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-aarch64-gnullvm-0.48
  (package
    (name "rust-windows-aarch64-gnullvm")
    (version "0.48.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_aarch64_gnullvm" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1g71yxi61c410pwzq05ld7si4p9hyx6lf5fkw21sinvr3cp5gbli"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-aarch64-msvc-0.33
  (package
    (name "rust-windows-aarch64-msvc")
    (version "0.33.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_aarch64_msvc" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "01q80v2zzfc144xsqj3yhd62rn1dy1kyamhyv0gcrf4sxg9iyxnd"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-aarch64-msvc-0.42
  (package
    (name "rust-windows-aarch64-msvc")
    (version "0.42.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_aarch64_msvc" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0hsdikjl5sa1fva5qskpwlxzpc5q9l909fpl1w6yy1hglrj8i3p0"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-aarch64-msvc-0.48
  (package
    (name "rust-windows-aarch64-msvc")
    (version "0.48.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_aarch64_msvc" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1wvwipchhywcjaw73h998vzachf668fpqccbhrxzrz5xszh2gvxj"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-i686-gnu-0.33
  (package
    (name "rust-windows-i686-gnu")
    (version "0.33.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_i686_gnu" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "03nsi8h0yd4n9wgpxcpynzwlnacihisgmh021kfb5fln79qczc6a"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-i686-gnu-0.42
  (package
    (name "rust-windows-i686-gnu")
    (version "0.42.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_i686_gnu" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0kx866dfrby88lqs9v1vgmrkk1z6af9lhaghh5maj7d4imyr47f6"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-i686-gnu-0.48
  (package
    (name "rust-windows-i686-gnu")
    (version "0.48.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_i686_gnu" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0hd2v9kp8fss0rzl83wzhw0s5z8q1b4875m6s1phv0yvlxi1jak2"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-i686-msvc-0.33
  (package
    (name "rust-windows-i686-msvc")
    (version "0.33.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_i686_msvc" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1l3kwxgdfg4lnx2j5bkcx6cnvhxnpcsbqjm3idhwxmwsrj4vxzcc"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-i686-msvc-0.42
  (package
    (name "rust-windows-i686-msvc")
    (version "0.42.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_i686_msvc" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0q0h9m2aq1pygc199pa5jgc952qhcnf0zn688454i7v4xjv41n24"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-i686-msvc-0.48
  (package
    (name "rust-windows-i686-msvc")
    (version "0.48.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_i686_msvc" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "004fkyqv3if178xx9ksqc4qqv8sz8n72mpczsr2vy8ffckiwchj5"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-x86-64-gnu-0.33
  (package
    (name "rust-windows-x86-64-gnu")
    (version "0.33.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_x86_64_gnu" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1127n961nib9338n0g0sp1464v8wnw0hvmw45sr7pkly1q69ppdl"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-x86-64-gnu-0.42
  (package
    (name "rust-windows-x86-64-gnu")
    (version "0.42.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_x86_64_gnu" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0dnbf2xnp3xrvy8v9mgs3var4zq9v9yh9kv79035rdgyp2w15scd"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-x86-64-gnu-0.48
  (package
    (name "rust-windows-x86-64-gnu")
    (version "0.48.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_x86_64_gnu" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1cblz5m6a8q6ha09bz4lz233dnq5sw2hpra06k9cna3n3xk8laya"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-x86-64-gnullvm-0.42
  (package
    (name "rust-windows-x86-64-gnullvm")
    (version "0.42.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_x86_64_gnullvm" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "18wl9r8qbsl475j39zvawlidp1bsbinliwfymr43fibdld31pm16"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-x86-64-gnullvm-0.48
  (package
    (name "rust-windows-x86-64-gnullvm")
    (version "0.48.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_x86_64_gnullvm" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0lxryz3ysx0145bf3i38jkr7f9nxiym8p3syklp8f20yyk0xp5kq"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-x86-64-msvc-0.33
  (package
    (name "rust-windows-x86-64-msvc")
    (version "0.33.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_x86_64_msvc" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1akf81g0bh8mv8wjpiifd819r0hx3r0xrz9zgzn4hl298sk4l7pz"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-x86-64-msvc-0.42
  (package
    (name "rust-windows-x86-64-msvc")
    (version "0.42.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_x86_64_msvc" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1w5r0q0yzx827d10dpjza2ww0j8iajqhmb54s735hhaj66imvv4s"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-windows-x86-64-msvc-0.48
  (package
    (name "rust-windows-x86-64-msvc")
    (version "0.48.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "windows_x86_64_msvc" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "12ipr1knzj2rwjygyllfi5mkd0ihnbi3r61gag5n2jgyk5bmyl8s"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-xflags-0.3
  (package
    (name "rust-xflags")
    (version "0.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "xflags" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1sn2y82vsjvmjd4j9rxg3pb28ig3dj7nphb9hciwml120mc4nmf4"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-xflags-macros" ,rust-xflags-macros-0.3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-xflags-macros-0.3
  (package
    (name "rust-xflags-macros")
    (version "0.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "xflags-macros" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1a6123c3qy1sqjsvlc357b2w8vr161vnlyxqwsm96w4pm0y7p3pm"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-xshell-0.2
  (package
    (name "rust-xshell")
    (version "0.2.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "xshell" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "10d9xi5751p3bjpb8dfxxwxrplfn5m1b6l8qwjqk8ln8qmyhjivd"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-xshell-macros" ,rust-xshell-macros-0.2))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-xshell-macros-0.2
  (package
    (name "rust-xshell-macros")
    (version "0.2.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "xshell-macros" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "06n406clanafsg2zppk78xvggwynha7m6n6q8dfbznbdq9b1nc48"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-xtask-0.1
  (package
    (name "rust-xtask")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "xtask" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0000000000000000000000000000000000000000000000000000"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-xshell" ,rust-xshell-0.2)
         ("rust-xflags" ,rust-xflags-0.3)
         ("rust-which" ,rust-which-4)
         ("rust-toml" ,rust-toml-0.5)
         ("rust-prost-build" ,rust-prost-build-0.11)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-anyhow" ,rust-anyhow-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-yaml-rust-0.4
  (package
    (name "rust-yaml-rust")
    (version "0.4.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "yaml-rust" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "118wbqrr4n6wgk5rjjnlrdlahawlxc1bdsx146mwk8f79in97han"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-linked-hash-map"
          ,rust-linked-hash-map-0.5))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))


(define rust-zellij-client-0.40
  (package
    (name "rust-zellij-client")
    (version "0.40.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "zellij-client" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0000000000000000000000000000000000000000000000000000"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-zellij-utils" ,rust-zellij-utils-0.40)
         ("rust-url" ,rust-url-2)
         ("rust-serde-yaml" ,rust-serde-yaml-0.8)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-mio" ,rust-mio-0.7)
         ("rust-log" ,rust-log-0.4)
         ("rust-insta" ,rust-insta-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-zellij-server-0.40
  (package
    (name "rust-zellij-server")
    (version "0.40.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "zellij-server" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0000000000000000000000000000000000000000000000000000"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-zellij-utils" ,rust-zellij-utils-0.40)
         ("rust-wasmer-wasi" ,rust-wasmer-wasi-3)
         ("rust-wasmer" ,rust-wasmer-3)
         ("rust-uuid" ,rust-uuid-1)
         ("rust-url" ,rust-url-2)
         ("rust-unicode-width" ,rust-unicode-width-0.1)
         ("rust-typetag" ,rust-typetag-0.1)
         ("rust-tempfile" ,rust-tempfile-3)
         ("rust-sysinfo" ,rust-sysinfo-0.22)
         ("rust-sixel-tokenizer"
          ,rust-sixel-tokenizer-0.1)
         ("rust-sixel-image" ,rust-sixel-image-0.1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-semver" ,rust-semver-0.11)
         ("rust-log" ,rust-log-0.4)
         ("rust-insta" ,rust-insta-1)
         ("rust-highway" ,rust-highway-0.6)
         ("rust-daemonize" ,rust-daemonize-0.4)
         ("rust-close-fds" ,rust-close-fds-0.3)
         ("rust-chrono" ,rust-chrono-0.4)
         ("rust-cassowary" ,rust-cassowary-0.3)
         ("rust-byteorder" ,rust-byteorder-1)
         ("rust-base64" ,rust-base64-0.13)
         ("rust-async-trait" ,rust-async-trait-0.1)
         ("rust-arrayvec" ,rust-arrayvec-0.7)
         ("rust-ansi-term" ,rust-ansi-term-0.12))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-zellij-tile-0.40
  (package
    (name "rust-zellij-tile")
    (version "0.40.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "zellij-tile" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0000000000000000000000000000000000000000000000000000"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-zellij-utils" ,rust-zellij-utils-0.40)
         ("rust-strum-macros" ,rust-strum-macros-0.20)
         ("rust-strum" ,rust-strum-0.20)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-clap" ,rust-clap-3))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-zellij-tile-utils-0.40
  (package
    (name "rust-zellij-tile-utils")
    (version "0.40.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "zellij-tile-utils" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0000000000000000000000000000000000000000000000000000"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-ansi-term" ,rust-ansi-term-0.12))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-zellij-utils-0.40
  (package
    (name "rust-zellij-utils")
    (version "0.40.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "zellij-utils" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0000000000000000000000000000000000000000000000000000"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-vte" ,rust-vte-0.11)
         ("rust-uuid" ,rust-uuid-1)
         ("rust-url" ,rust-url-2)
         ("rust-unicode-width" ,rust-unicode-width-0.1)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-termwiz" ,rust-termwiz-0.20)
         ("rust-tempfile" ,rust-tempfile-3)
         ("rust-surf" ,rust-surf-2)
         ("rust-strum-macros" ,rust-strum-macros-0.20)
         ("rust-strum" ,rust-strum-0.20)
         ("rust-strip-ansi-escapes"
          ,rust-strip-ansi-escapes-0.1)
         ("rust-signal-hook" ,rust-signal-hook-0.3)
         ("rust-shellexpand" ,rust-shellexpand-3)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-rmp-serde" ,rust-rmp-serde-1)
         ("rust-regex" ,rust-regex-1)
         ("rust-prost-build" ,rust-prost-build-0.11)
         ("rust-prost" ,rust-prost-0.11)
         ("rust-percent-encoding"
          ,rust-percent-encoding-2)
         ("rust-openssl-sys" ,rust-openssl-sys-0.9)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-notify-debouncer-full"
          ,rust-notify-debouncer-full-0.1)
         ("rust-nix" ,rust-nix-0.23)
         ("rust-miette" ,rust-miette-5)
         ("rust-log4rs" ,rust-log4rs-1)
         ("rust-log" ,rust-log-0.4)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-kdl" ,rust-kdl-4)
         ("rust-interprocess" ,rust-interprocess-1)
         ("rust-insta" ,rust-insta-1)
         ("rust-include-dir" ,rust-include-dir-0.7)
         ("rust-humantime" ,rust-humantime-2)
         ("rust-futures" ,rust-futures-0.3)
         ("rust-expect-test" ,rust-expect-test-1)
         ("rust-directories" ,rust-directories-5)
         ("rust-crossbeam" ,rust-crossbeam-0.8)
         ("rust-common-path" ,rust-common-path-1)
         ("rust-colorsys" ,rust-colorsys-0.6)
         ("rust-colored" ,rust-colored-2)
         ("rust-clap-complete" ,rust-clap-complete-3)
         ("rust-clap" ,rust-clap-3)
         ("rust-backtrace" ,rust-backtrace-0.3)
         ("rust-async-std" ,rust-async-std-1)
         ("rust-async-channel" ,rust-async-channel-1)
         ("rust-anyhow" ,rust-anyhow-1))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define rust-zeroize-1
  (package
    (name "rust-zeroize")
    (version "1.5.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "zeroize" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "01yzgi5n5skl7l8wypbpzw8r6s6azhxyn824w79g5chns03khscl"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))

(define zellij
  (package
    (name "zellij")
    (version "0.40.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "zellij" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0000000000000000000000000000000000000000000000000000"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-zellij-utils" ,rust-zellij-utils-0.40)
         ("rust-zellij-server" ,rust-zellij-server-0.40)
         ("rust-zellij-client" ,rust-zellij-client-0.40)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-suggest" ,rust-suggest-0.4)
         ("rust-ssh2" ,rust-ssh2-0.9)
         ("rust-regex" ,rust-regex-1)
         ("rust-rand" ,rust-rand-0.8)
         ("rust-names" ,rust-names-0.14)
         ("rust-log" ,rust-log-0.4)
         ("rust-insta" ,rust-insta-1)
         ("rust-dialoguer" ,rust-dialoguer-0.10))))
    (home-page "")
    (synopsis "")
    (description "")
    (license #f)))
zellij
