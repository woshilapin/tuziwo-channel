(define-module (tuziwo packages rust)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages crates-io)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages gdb)
  #:use-module (gnu packages libffi)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages llvm)
  #:use-module (gnu packages llvm-meta)
  #:use-module (gnu packages rust)
  #:use-module (gnu packages web)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix build-system cargo)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix platform)
  #:use-module (guix utils)
  #:use-module (srfi srfi-1))

(define rust-bootstrapped-package
  (@@ (gnu packages rust) rust-bootstrapped-package))
(define make-ignore-test-list
  (@@ (gnu packages rust) make-ignore-test-list))

(define-public rust-1.82
    (package
      (inherit rust)
      (arguments
       (substitute-keyword-arguments
         (strip-keyword-arguments '(#:tests?)
           (package-arguments rust))
         ((#:phases phases)
          `(modify-phases ,phases
            ;; https://rustc-dev-guide.rust-lang.org/building/how-to-build-and-run.html#creating-a-rustup-toolchain
             (replace 'build
               ;; Phase overridden to also build more tools.
               (lambda* (#:key parallel-build? #:allow-other-keys)
                 (let ((job-spec (string-append
                                  "-j" (if parallel-build?
                                           (number->string (parallel-job-count))
                                           "1"))))
                   (invoke "./x.py" job-spec "build"
                           "library/std" ;rustc
                           "src/tools/cargo"
                           "src/tools/clippy"
                           "src/tools/rust-analyzer"
                           "src/tools/rustfmt"
                           ;; https://rustc-dev-guide.rust-lang.org/building/how-to-build-and-run.html#creating-a-rustup-toolchain
                           "proc-macro-srv-cli"))))
             (add-after 'install 'install-rust-analyzer-proc-macro-srv
               (lambda* (#:key outputs #:allow-other-keys)
                 (let ((out (assoc-ref outputs "tools"))
                       (target ,(platform-rust-target
                               (lookup-platform-by-target-or-system
                                 (or (%current-target-system)
                                     (%current-system))))))
                   (copy-file (string-append "build/" target "/stage2-tools/" target "/release/rust-analyzer-proc-macro-srv") (string-append out "/bin/rust-analyzer-proc-macro-srv")))))
             (add-after 'wrap-rust-analyzer 'wrap-rust-analyzer-proc-macro-srv
               (lambda* (#:key outputs #:allow-other-keys)
                 (let ((bin (string-append (assoc-ref outputs "tools") "/bin")))
                   (rename-file (string-append bin "/rust-analyzer-proc-macro-srv")
                                (string-append bin "/.rust-analyzer-proc-macro-srv-real"))
                   (call-with-output-file (string-append bin "/rust-analyzer-proc-macro-srv")
                     (lambda (port)
                       (format port "#!~a
if test -z \"${RUST_SRC_PATH}\";then export RUST_SRC_PATH=~S;fi;
exec -a \"$0\" \"~a\" \"$@\""
                               (which "bash")
                               (string-append (assoc-ref outputs "rust-src")
                                              "/lib/rustlib/src/rust/library")
                               (string-append bin "/.rust-analyzer-proc-macro-srv-real"))))
                   (chmod (string-append bin "/rust-analyzer-proc-macro-srv") #o755))))))))))

rust-1.82
