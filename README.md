tuziwo channel for guix
=====

My personal [guix](https://guix.gnu.org/) channel.

# Usage

Add the following to your 'channels.scm'.

```scheme
(channel
  (name 'tuziwo)
  (branch "main")
  (url "https://gitlab.com/woshilapin/tuziwo-channel")
  (introduction
    (make-channel-introduction
      "0deff2a94032f2d96e82f93edeb61f35da879987"
      (openpgp-fingerprint
       "5554 54E7 6611 9F60 80F1  2F63 B041 63DC 7020 116A"))))
```
